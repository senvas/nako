#![cfg_attr(
    target_arch = "spirv",
    no_std,
    feature(register_attr),
    register_attr(spirv)
)]
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

use shared::glam::{UVec3, Vec3, Vec3Swizzles, Vec4};
use shared::spirv_std;
use shared::PrimaryConstants;
//use shared::test_sdf::eval;

#[cfg(not(target_arch = "spirv"))]
use spirv_std::{macros::spirv, Image};

#[cfg(target_arch = "spirv")]
use spirv_std::{num_traits::float::Float, Image};

// LocalSize/numthreads of (x = 32, y = 1, z = 1)
#[spirv(compute(threads(8, 8)))]
pub fn main(
    #[spirv(push_constant)] constants: &PrimaryConstants,
    #[spirv(descriptor_set = 0, binding = 0)] target_image: &Image!(2D, format=rgba32f, sampled=false),
    #[spirv(storage_buffer, descriptor_set = 0, binding = 1)] _slice: &mut [u8],
    #[spirv(global_invocation_id)] id: UVec3,
) {
    let coord = id.xy();
    if coord.x > constants.camera.img_width || coord.y > constants.camera.img_height {
        //Do not write outside of image
        return;
    }

    //let ray = constants.camera.primary_ray(coord.x, coord.y);
    let color = Vec3::new(1.0, 1.0, 0.0);

    unsafe {
        target_image.write(coord, Vec4::new(color.x, color.y, color.z, 1.0));
    }
}
