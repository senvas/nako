#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![feature(unchecked_math)]
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

pub use glam;
use glam::{Mat4, Vec3, Vec4, Vec4Swizzles};

pub use spirv_std;
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::float::Float;

pub use nako;

///Gpu aligned version of `camera`.
#[repr(C)]
#[derive(Clone)]
pub struct GpuCamera {
    pub location: [f32; 3],
    pub fov: f32,
    pub transform: [[f32; 4]; 4],
    pub img_width: u32,
    pub img_height: u32,
    pub pad1: [u32; 2],
}

impl GpuCamera {
    //Creates a primary ray based on pixel positions x,y
    pub fn primary_ray(&self, x: u32, y: u32) -> Ray {
        let aspect_ratio = self.img_width as f32 / self.img_height as f32;
        let px = (2.0 * ((x as f32 + 0.5) / self.img_width as f32) - 1.0)
            * (self.fov / 2.0 * core::f32::consts::PI / 180.0).tan()
            * aspect_ratio;

        let py = (1.0 - 2.0 * ((y as f32 + 0.5) / self.img_height as f32))
            * (self.fov / 2.0 * core::f32::consts::PI / 180.0).tan();

        let transform: Mat4 = Mat4::from_cols_array_2d(&self.transform);
        let origin: Vec4 = transform * Vec4::new(0.0, 0.0, 0.0, 1.0);
        let direction: Vec4 = transform * Vec4::new(px, py, 1.0, 0.0);

        Ray {
            origin: (origin.xyz() / origin.w),
            direction: direction.xyz(),
        }
    }
}

///2d Camera used when querying a 2d sdf.
#[repr(C)]
#[derive(Clone, Debug)]
pub struct GpuCamera2d {
    pub location: [f32; 2],
    pub extent: [f32; 2],
    pub pad1: [f32; 3],
    pub rotation: f32,
}

#[repr(C)]
pub struct PrimaryConstants {
    pub camera: GpuCamera,
}

#[repr(C)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}
