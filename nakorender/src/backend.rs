/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use marp_surface_winit::winit::window::Window;
use nako::stream::{PrimaryStream, PrimaryStream2d};

use crate::camera::{Camera, Camera2d};

///Id to any type of layer.
#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
pub enum LayerId {
    Planar(LayerId2d),
    Volumetric(LayerId3d),
}

impl From<LayerId2d> for LayerId {
    fn from(id: LayerId2d) -> Self {
        LayerId::Planar(id)
    }
}

impl From<LayerId3d> for LayerId {
    fn from(id: LayerId3d) -> Self {
        LayerId::Volumetric(id)
    }
}
///Id for an layer presenting an planar SDF
#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
pub struct LayerId2d(pub(crate) usize);
#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
///Id for an layer presenting an volumetric SDF
pub struct LayerId3d(pub(crate) usize);

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct LayerInfo {
    pub location: (usize, usize),
    pub extent: (usize, usize),
}

///Defines the interface to any backend.
pub trait Backend {
    fn new(
        window: &Window,
        event_loop: &marp_surface_winit::winit::event_loop::EventLoop<()>,
    ) -> Self;

    ///Allocates a new 3d sdf representing layer
    fn new_layer(&mut self) -> LayerId3d;
    ///Allocates a new 2d sdf representing layer
    fn new_layer_2d(&mut self) -> LayerId2d;

    ///Tells the backend that the sdf tree has changed for the given 3d layer.
    fn update_sdf(&mut self, id: LayerId3d, new_sdf: PrimaryStream);
    ///Updates the camera of the given 3d layer
    fn update_camera(&mut self, id: LayerId3d, camera: Camera);

    ///Tells the backend that the sdf tree has changed for the given 3d layer.
    fn update_sdf_2d(&mut self, id: LayerId2d, new_sdf: PrimaryStream2d);
    ///Updates the camera of the given 3d layer
    fn update_camera_2d(&mut self, id: LayerId2d, camera: Camera2d);

    ///Sets location and extent of the given layer within the renderer.
    fn set_layer_info(&mut self, id: LayerId, info: LayerInfo);

    ///Sets the order in which the given layers are combined, where first is the lowest and last is the highest.
    fn set_layer_order(&mut self, order: &[LayerId]);

    ///Tells the renderer to render a new frame into `window`.
    fn render(&mut self, window: &Window);
    ///Resizes the renderers internal window size
    fn resize(&mut self, window: &Window);
}
