/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::{collections::HashMap, sync::Arc};

use image::{Rgba, RgbaImage};
use nako::stream::{PrimaryStream, PrimaryStream2d};

use crate::{
    backend::{LayerId, LayerId2d, LayerId3d, LayerInfo},
    camera::{Camera, Camera2d},
};

use super::rendering::{evaluate_sdf_2d, evaluate_sdf_3d};

fn mix_alpha(a: f32, b: f32, alpha: f32) -> f32 {
    let alpha = alpha.clamp(0.0, 1.0);

    a * alpha + b * (1.0 - alpha)
}

fn mix(target: Rgba<u8>, src: Rgba<u8>) -> Rgba<u8> {
    Rgba([
        mix_alpha(src.0[0] as f32, target.0[0] as f32, src.0[3] as f32).clamp(0.0, u8::MAX as f32)
            as u8,
        mix_alpha(src.0[1] as f32, target.0[1] as f32, src.0[3] as f32).clamp(0.0, u8::MAX as f32)
            as u8,
        mix_alpha(src.0[2] as f32, target.0[2] as f32, src.0[3] as f32).clamp(0.0, u8::MAX as f32)
            as u8,
        src.0[3].max(target.0[3]),
    ])
}

///Blends the two images based on the alpha of src
fn mix_image(target: &mut RgbaImage, src: &RgbaImage, src_info: &LayerInfo) {
    //early return if nothing to be copied
    if (target.width() as usize) < src_info.location.0
        || (target.height() as usize) < src_info.location.1
    {
        return;
    }

    let copy_width = (src_info.location.0 + src_info.extent.0).min(target.width() as usize)
        - src_info.location.0;
    let copy_height = (src_info.location.1 + src_info.extent.1).min(target.height() as usize)
        - src_info.location.1;

    println!(
        "Target: {}x{}, range: {}x{} - {}x{}",
        target.width(),
        target.height(),
        src_info.location.0,
        src_info.location.1,
        copy_width,
        copy_height
    );

    for x in 0..copy_width {
        for y in 0..copy_height {
            *target.get_pixel_mut(
                src_info.location.0 as u32 + x as u32,
                src_info.location.1 as u32 + y as u32,
            ) = mix(
                *target.get_pixel(
                    src_info.location.0 as u32 + x as u32,
                    src_info.location.1 as u32 + y as u32,
                ),
                *src.get_pixel(x as u32, y as u32),
            );
        }
    }
}

struct Layer3d {
    sdf: Arc<PrimaryStream>,
    camera: Camera,
    info: LayerInfo,
    //cached image
    img: RgbaImage,
}

impl Layer3d {
    fn render(&mut self) {
        self.img = evaluate_sdf_3d(
            self.info.extent.0,
            self.info.extent.1,
            self.sdf.clone(),
            &self.camera,
        );
    }
    fn set_sdf(&mut self, new_sdf: PrimaryStream) {
        self.sdf = Arc::new(new_sdf);
        self.render();
    }
    fn set_camera(&mut self, camera: Camera) {
        self.camera = camera;
        self.render();
    }
    fn set_info(&mut self, new_info: LayerInfo) {
        let should_render = self.info.extent != new_info.extent;
        self.info = new_info;
        if should_render {
            //rerender if the extent changed
            self.render();
        }
    }

    fn blend_into(&self, target: &mut RgbaImage) {
        mix_image(target, &self.img, &self.info);
    }
}

struct Layer2d {
    sdf: Arc<PrimaryStream2d>,
    camera: Camera2d,
    info: LayerInfo,
    img: RgbaImage,
}

impl Layer2d {
    fn render(&mut self) {
        println!(
            "Render from {} to {} @ {}x{}",
            self.camera
                .pixel_to_worldspace(0, 0, self.info.extent.0, self.info.extent.1),
            self.camera.pixel_to_worldspace(
                self.info.extent.0,
                self.info.extent.1,
                self.info.extent.0,
                self.info.extent.1
            ),
            self.info.extent.0,
            self.info.extent.1
        );
        self.img = evaluate_sdf_2d(
            self.info.extent.0,
            self.info.extent.1,
            self.sdf.clone(),
            &self.camera,
        );
    }
    fn set_sdf(&mut self, new_sdf: PrimaryStream2d) {
        self.sdf = Arc::new(new_sdf);
        self.render();
    }
    fn set_camera(&mut self, camera: Camera2d) {
        self.camera = camera;
        self.render();
    }
    fn set_info(&mut self, new_info: LayerInfo) {
        let should_render = self.info.extent != new_info.extent;
        self.info = new_info;
        if should_render {
            //rerender if the extent changed
            self.render();
        }
    }

    fn blend_into(&self, target: &mut RgbaImage) {
        mix_image(target, &self.img, &self.info);
    }
}

enum Layer {
    L3d(Layer3d),
    L2d(Layer2d),
}

pub(crate) struct LayerManager {
    layers: HashMap<LayerId, Layer>,
    order: Vec<LayerId>,
}

impl LayerManager {
    pub(crate) fn new() -> Self {
        LayerManager {
            layers: HashMap::new(),
            order: vec![],
        }
    }

    pub(crate) fn set_info(&mut self, id: &LayerId, info: LayerInfo) {
        if let Some(layer) = self.layers.get_mut(id) {
            match layer {
                Layer::L3d(l) => l.set_info(info),
                Layer::L2d(l) => l.set_info(info),
            }
        }
    }

    pub(crate) fn set_sdf(&mut self, id: LayerId3d, sdf: PrimaryStream) {
        if let Some(layer) = self.layers.get_mut(&LayerId::Volumetric(id)) {
            match layer {
                Layer::L3d(l) => l.set_sdf(sdf),
                _ => {}
            }
        }
    }

    pub(crate) fn set_camera(&mut self, id: LayerId3d, camera: Camera) {
        if let Some(layer) = self.layers.get_mut(&LayerId::Volumetric(id)) {
            match layer {
                Layer::L3d(l) => l.set_camera(camera),
                _ => {}
            }
        }
    }

    pub(crate) fn set_sdf_2d(&mut self, id: LayerId2d, sdf: PrimaryStream2d) {
        if let Some(layer) = self.layers.get_mut(&LayerId::Planar(id)) {
            match layer {
                Layer::L2d(l) => l.set_sdf(sdf),
                _ => {}
            }
        }
    }

    pub(crate) fn set_camera_2d(&mut self, id: LayerId2d, camera: Camera2d) {
        if let Some(layer) = self.layers.get_mut(&LayerId::Planar(id)) {
            match layer {
                Layer::L2d(l) => l.set_camera(camera),
                _ => {}
            }
        }
    }

    pub fn alloc_id_3d(&mut self) -> LayerId3d {
        let mut id = 0;
        while self
            .layers
            .contains_key(&LayerId::Volumetric(LayerId3d(id)))
        {
            id += 1;
        }

        self.layers.insert(
            LayerId::Volumetric(LayerId3d(id)),
            Layer::L3d(Layer3d {
                camera: Camera::default(),
                img: RgbaImage::new(1, 1),
                info: LayerInfo {
                    extent: (1, 1),
                    location: (0, 0),
                },
                sdf: Arc::new(PrimaryStream::new().build()),
            }),
        );

        LayerId3d(id)
    }

    pub fn alloc_id_2d(&mut self) -> LayerId2d {
        let mut id = 0;
        while self.layers.contains_key(&LayerId::Planar(LayerId2d(id))) {
            id += 1;
        }

        self.layers.insert(
            LayerId::Planar(LayerId2d(id)),
            Layer::L2d(Layer2d {
                camera: Camera2d::default(),
                img: RgbaImage::new(1, 1),
                info: LayerInfo {
                    extent: (1, 1),
                    location: (0, 0),
                },
                sdf: Arc::new(PrimaryStream2d::new().build()),
            }),
        );

        LayerId2d(id)
    }

    pub fn set_order(&mut self, order: Vec<LayerId>) {
        self.order = order;
    }

    ///Renders a new image from the layers and returns it.
    pub fn new_image(&self, width: usize, height: usize) -> Vec<u8> {
        //Init empty image
        let mut final_image = RgbaImage::new(width as u32, height as u32);

        for id in &self.order {
            if let Some(layer) = self.layers.get(id) {
                match layer {
                    Layer::L3d(l) => l.blend_into(&mut final_image),
                    Layer::L2d(l) => l.blend_into(&mut final_image),
                }
            }
        }

        final_image.into_raw()
    }
}
