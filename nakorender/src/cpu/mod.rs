/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use crate::{
    backend::{Backend, LayerId, LayerId2d, LayerId3d, LayerInfo},
    camera::{Camera, Camera2d},
};
use marp_surface_winit::winit;
use nako::stream::{PrimaryStream, PrimaryStream2d};
use pixels::{Pixels, SurfaceTexture};
use std::{
    sync::{
        mpsc::{Receiver, Sender},
        Arc, Mutex,
    },
    thread::JoinHandle,
    time::Instant,
};
use winit::window::Window;

use self::layering::LayerManager;

pub(crate) mod evaluator;
mod layering;
mod rendering;
mod shaders;

enum RenderMsg {
    New3dSdf(LayerId3d, PrimaryStream),
    New2dSdf(LayerId2d, PrimaryStream2d),
    Camera3d(LayerId3d, Camera),
    Camera2d(LayerId2d, Camera2d),
    NewInfo(LayerId, LayerInfo),
    NewOrder(Vec<LayerId>),
    NewId2d,
    NewId3d,
    Render,
}

///The renderer. mostly handles the displaying of new content
pub struct CpuBackend {
    extent: Arc<Mutex<(u32, u32)>>,
    pixels: Pixels<Window>,
    //If some, currently calculating a new frame
    frame_recv: Receiver<Vec<u8>>,
    id_receiver: Receiver<LayerId>,
    scene_sender: Sender<RenderMsg>,
    stop_flag: Arc<Mutex<bool>>,
    render_thread: Option<JoinHandle<()>>,
}

impl Backend for CpuBackend {
    fn new(window: &winit::window::Window, _event_loop: &winit::event_loop::EventLoop<()>) -> Self {
        let ext = window.inner_size();
        let extent = Arc::new(Mutex::new((ext.width, ext.height)));
        let pixels = {
            let window_size = window.inner_size();
            let surface_texture =
                SurfaceTexture::new(window_size.width, window_size.height, window);
            Pixels::new(ext.width, ext.height, surface_texture).unwrap()
        };

        let (frame_sender, frame_recv) = std::sync::mpsc::channel();
        let (scene_sender, scene_recv): (Sender<RenderMsg>, _) = std::sync::mpsc::channel();
        let (id_sender, id_recv): (Sender<LayerId>, _) = std::sync::mpsc::channel();
        let frame_stop = Arc::new(Mutex::new(false));

        let render_thread = std::thread::spawn({
            let should_stop = frame_stop.clone();
            let extent = extent.clone();
            let mut layer_manager = LayerManager::new();
            move || {
                while !*should_stop.lock().unwrap() {
                    if let Ok(new_event) = scene_recv.try_recv() {
                        match new_event {
                            RenderMsg::NewInfo(id, info) => layer_manager.set_info(&id, info),
                            RenderMsg::New3dSdf(id, sdf) => layer_manager.set_sdf(id, sdf),
                            RenderMsg::New2dSdf(id, sdf) => layer_manager.set_sdf_2d(id, sdf),
                            RenderMsg::Camera3d(id, camera) => layer_manager.set_camera(id, camera),
                            RenderMsg::Camera2d(id, camera) => {
                                layer_manager.set_camera_2d(id, camera)
                            }
                            RenderMsg::NewOrder(order) => layer_manager.set_order(order),
                            RenderMsg::NewId3d => id_sender
                                .send(LayerId::Volumetric(layer_manager.alloc_id_3d()))
                                .unwrap(),
                            RenderMsg::NewId2d => id_sender
                                .send(LayerId::Planar(layer_manager.alloc_id_2d()))
                                .unwrap(),
                            RenderMsg::Render => {
                                let start = Instant::now();
                                let (x, y) = *extent.lock().unwrap();
                                let new_frame = layer_manager.new_image(x as usize, y as usize);
                                println!(
                                    "new frame took {:.2}ms",
                                    start.elapsed().as_secs_f32() * 1000.0
                                );
                                frame_sender.send(new_frame).unwrap();
                            }
                        }
                    }
                }
            }
        });

        CpuBackend {
            extent,
            pixels,
            frame_recv,
            id_receiver: id_recv,
            scene_sender,
            stop_flag: frame_stop,
            render_thread: Some(render_thread),
        }
    }

    fn new_layer(&mut self) -> LayerId3d {
        self.scene_sender.send(RenderMsg::NewId3d).unwrap();
        match self.id_receiver.recv().unwrap() {
            LayerId::Volumetric(id) => id,
            _ => panic!("Got wrong id"),
        }
    }

    fn new_layer_2d(&mut self) -> LayerId2d {
        self.scene_sender.send(RenderMsg::NewId2d).unwrap();
        match self.id_receiver.recv().unwrap() {
            LayerId::Planar(id) => id,
            _ => panic!("Got wrong id"),
        }
    }

    fn update_sdf(&mut self, id: LayerId3d, new_sdf: PrimaryStream) {
        self.scene_sender
            .send(RenderMsg::New3dSdf(id, new_sdf))
            .unwrap();
    }
    fn update_camera(&mut self, id: LayerId3d, camera: Camera) {
        self.scene_sender
            .send(RenderMsg::Camera3d(id, camera))
            .unwrap();
    }

    fn update_sdf_2d(&mut self, id: LayerId2d, new_sdf: PrimaryStream2d) {
        self.scene_sender
            .send(RenderMsg::New2dSdf(id, new_sdf))
            .unwrap();
    }
    fn update_camera_2d(&mut self, id: LayerId2d, camera: Camera2d) {
        self.scene_sender
            .send(RenderMsg::Camera2d(id, camera))
            .unwrap();
    }

    fn set_layer_info(&mut self, id: LayerId, info: LayerInfo) {
        self.scene_sender
            .send(RenderMsg::NewInfo(id, info))
            .unwrap();
    }

    fn set_layer_order(&mut self, order: &[LayerId]) {
        self.scene_sender
            .send(RenderMsg::NewOrder(order.to_vec()))
            .unwrap();
    }

    fn render(&mut self, _window: &winit::window::Window) {
        self.scene_sender.send(RenderMsg::Render).unwrap();
        //Wait for the frame
        if let Ok(buf) = self.frame_recv.recv() {
            let min = buf.len().min(self.pixels.get_frame().len());
            let numpx = {
                let ex = self.extent.lock().unwrap();
                ex.0 * ex.1
            };
            println!(
                "Copying to {}px, should be {}, buf={}, frame={}",
                min / 4,
                numpx,
                buf.len() / 4,
                self.pixels.get_frame().len() / 4
            );
            self.pixels.get_frame()[0..min].copy_from_slice(&buf[0..min]);

            //Flag as updated
            if let Err(e) = self.pixels.render() {
                println!("Failed to render: {}", e);
            } else {
                println!("updated");
            }
        }
    }

    fn resize(&mut self, window: &Window) {
        self.pixels = {
            let window_size = window.inner_size();
            let surface_texture =
                SurfaceTexture::new(window_size.width, window_size.height, window);
            Pixels::new(window_size.width, window_size.height, surface_texture).unwrap()
        };
        println!("Resized to {:?}", window.inner_size());
        *self.extent.lock().unwrap() = (window.inner_size().width, window.inner_size().height);
    }
}

impl Drop for CpuBackend {
    fn drop(&mut self) {
        *self.stop_flag.lock().unwrap() = true;
        self.render_thread.take().unwrap().join().unwrap();
    }
}
