/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use crate::camera::{Camera, Camera2d};
use crate::cpu::shaders::{lit, DEBUG, MAX_ITER};
use colorgrad::Color;
use image::Rgba;
use image::{self, RgbaImage};
use nako::stream::PrimaryStream;
use nako::stream::PrimaryStream2d;
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;
use std::{sync::Arc, time::Instant};

use super::shaders::unlit_2d;

pub fn evaluate_sdf_3d(
    img_width: usize,
    img_height: usize,
    code: Arc<PrimaryStream>,
    camera: &Camera,
) -> RgbaImage {
    let mut img = RgbaImage::new(img_width as u32, img_height as u32);

    let code = code.clone();
    let start = Instant::now();

    let pixel: Vec<(u32, u32, Rgba<f32>)> = (0..img_width)
        .into_par_iter()
        .map(|x| {
            let mut result = Vec::with_capacity(img_height);
            let code = code.clone();

            for y in 0..img_height {
                let color = lit((img_width, img_height), (x, y), *camera, code.clone());
                result.push((x as u32, y as u32, color));
            }
            result
        })
        .flatten()
        .collect();

    println!(
        "Rendering took {}ms",
        start.elapsed().as_secs_f32() * 1000.0
    );

    let grad = colorgrad::CustomGradient::new()
        .colors(&[
            Color::from_rgb_u8(0, 0, 255),
            Color::from_rgb_u8(0, 255, 0),
            Color::from_rgb_u8(255, 0, 0),
        ])
        .domain(&[0.0, MAX_ITER as f64])
        .build()
        .unwrap();

    for (x, y, color) in pixel.into_iter() {
        let mut px = img.get_pixel_mut(x, y);

        if DEBUG {
            let (r, g, b, _a) = grad.at(color[3] as f64).rgba_u8();

            px.0[0] = r;
            px.0[1] = g;
            px.0[2] = b;
            px.0[3] = (color.0[3] * u8::MAX as f32) as u8;
        } else {
            px.0[0] = (color.0[0] * u8::MAX as f32) as u8;
            px.0[1] = (color.0[1] * u8::MAX as f32) as u8;
            px.0[2] = (color.0[2] * u8::MAX as f32) as u8;
            px.0[3] = (color.0[3] * u8::MAX as f32) as u8;
        }
    }

    img
}

pub fn evaluate_sdf_2d(
    img_width: usize,
    img_height: usize,
    code: Arc<PrimaryStream2d>,
    camera: &Camera2d,
) -> RgbaImage {
    let mut img = RgbaImage::new(img_width as u32, img_height as u32);

    let pixel: Vec<(u32, u32, Rgba<f32>)> = (0..img_width)
        .into_par_iter()
        .map(|x| {
            let mut result = Vec::with_capacity(img_height);
            let code = code.clone();

            for y in 0..img_height {
                let color = unlit_2d((img_width, img_height), (x, y), *camera, code.clone());
                result.push((x as u32, y as u32, color));
            }
            result
        })
        .flatten()
        .collect();

    //Collect pixel into image buffer
    for (x, y, color) in pixel.into_iter() {
        let mut px = img.get_pixel_mut(x, y);
        px.0[0] = (color.0[0] * u8::MAX as f32) as u8;
        px.0[1] = (color.0[1] * u8::MAX as f32) as u8;
        px.0[2] = (color.0[2] * u8::MAX as f32) as u8;
        px.0[3] = (color.0[3] * u8::MAX as f32) as u8;
    }

    img
}
