/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::sync::Arc;

use image::Rgba;
use nako::{
    glam::{Vec2, Vec2Swizzles, Vec3, Vec4, Vec4Swizzles},
    serialize::Word,
    stream::{PrimaryStream, PrimaryStream2d},
};

use crate::camera::{Camera, Camera2d};

use super::evaluator::{eval_2d_from_code, eval_from_code};

const MAX_DISTANCE: f32 = 1000.0;
pub const MAX_ITER: usize = 512;
///How exact the evaluation is
const THREASOLD: f32 = 0.001;
///How exact shadow evaluation is
#[allow(dead_code)]
const SHADOW_THREASOLD: f32 = 0.001;
///How sharp the shadows are
#[allow(dead_code)]
const SHADOW_SHARPNESS: f32 = 17.0;
///Delta used to shoot normal rays
const NRM_DELTA: f32 = 0.005;

pub const DEBUG: bool = false;

const LIGHT_LOCATION: [f32; 3] = [0.0, 30.0, 00.0];
const AMBIENT_LIGHT: [f32; 3] = [0.06, 0.05, 0.05];
const LIGHT_COLOR: [f32; 3] = [1.0, 0.95, 0.95];

///The last sdf evaluation distance, or -1
fn sphere_march(direction: Vec3, origin: Vec3, code: &[Word]) -> (f32, Vec3, usize) {
    let mut t = 0.0;
    let mut num_iter = 0;

    while t < MAX_DISTANCE && num_iter < MAX_ITER {
        num_iter += 1;
        let new_point = origin + direction * t;
        let eval = eval_from_code(new_point, code);

        if eval.result < THREASOLD {
            return (t, eval.color, num_iter);
        } else {
            t += eval.result;
        }
    }

    (t, Vec3::ZERO, num_iter)
}
/* Stuff needed for segment tracing
 * However I did not find a performant way to do that just yet.
const EPS: f32 = 0.0001;
const GLOBAL_K: f32 = 1.25;
fn calc_gradient_dir_abs(pos: Vec3, dir: Vec3, code: &[Word]) -> f32 {
    (eval_from_code(pos + dir * EPS, code).result - eval_from_code(pos - dir * EPS, code).result)
        .abs()
        / (2.0 / EPS)
}

///Calculates scenes libschitz bound in region
fn scene_k(start: Vec3, end: Vec3, dir: Vec3, code: &[Word]) -> (f32, NakoResult) {
    let dist = eval_from_code(start, code);

    let fds = calc_gradient_dir_abs(start, dir, code);
    let fde = calc_gradient_dir_abs(end, dir, code);
    let lam = fds.max(fde);
    (lam, dist)
}

fn segment_trace(direction: Vec3, origin: Vec3, code: &[Word]) -> (f32, Vec3, usize) {
    let mut t = 0.0;
    let mut num_iter = 0;
    let mut num_evals = 0;
    let c = 1.5;
    let libschitz_grid_size = 0.4;
    let mut ts = libschitz_grid_size; //Start segment size

    while t < MAX_DISTANCE && num_iter < MAX_ITER {
        num_iter += 1;
        let p = origin + direction * t;
        let ps = origin + direction * (t + ts);

        let (lam, res) = scene_k(p, ps, direction, code);
        num_evals += 5;
        if res.result < 0.0 {
            return (t, res.color, num_evals);
        }

        let tk = res.result.abs() / lam.max(0.01);
        let tk = (res.result.abs() / GLOBAL_K).max(tk.min(ts));

        if tk >= 0.0 {
            t += tk.max(EPS);
        }
        ts = tk * c;
        ts = ts.min(libschitz_grid_size);
    }
    (f32::INFINITY, Vec3::ZERO, num_iter)
}
*/
///Calculates shadow by returning how much light is occluded
#[allow(dead_code)]
fn shadow_march(start: Vec3, light_location: Vec3, sharpness: f32, code: &[Word]) -> (f32, usize) {
    let mut res = 1.0 as f32;
    let dir = (light_location - start).normalize();
    let max_t = (light_location - start).length();
    let mut t = 0.0;

    let mut num_iter = 0;

    while t < max_t && num_iter < MAX_ITER {
        num_iter += 1;
        let point = start + (dir * t);
        let eval = eval_from_code(point, code);
        //Easy out when plain intersecting
        if eval.result < SHADOW_THREASOLD {
            return (0.0, num_iter);
        }

        res = res.min(sharpness * eval.result / t);
        t += eval.result;
    }
    (res, num_iter)
}

pub fn calc_normal(code: &[Word], p: Vec3) -> Vec3 {
    let k = Vec2::new(1.0, -1.0);
    (k.xyy() * eval_from_code(p + k.xyy() * NRM_DELTA, code).result
        + k.yyx() * eval_from_code(p + k.yyx() * NRM_DELTA, code).result
        + k.yxy() * eval_from_code(p + k.yxy() * NRM_DELTA, code).result
        + k.xxx() * eval_from_code(p + k.xxx() * NRM_DELTA, code).result)
        .normalize()
}

///renders a pixel at `coord`, based on `camera` and an sdf.
pub fn lit(
    (img_width, img_height): (usize, usize),
    (x, y): (usize, usize),
    camera: Camera,
    sdf: Arc<PrimaryStream>,
) -> Rgba<f32> {
    const FOV: f32 = 75.0;
    let aspect_ratio = img_width as f32 / img_height as f32;

    //===Pixel Shader====
    let px = (2.0 * ((x as f32 + 0.5) / img_width as f32) - 1.0)
        * (FOV / 2.0 * core::f32::consts::PI / 180.0).tan()
        * aspect_ratio;
    let py = (1.0 - 2.0 * ((y as f32 + 0.5) / img_height as f32))
        * (FOV / 2.0 * core::f32::consts::PI / 180.0).tan();

    let origin = camera.transform() * Vec4::new(0.0, 0.0, 0.0, 1.0);
    let direction = camera.transform() * Vec4::new(px, py, 1.0, 0.0);

    let origin = origin.xyz() / origin.w;
    let direction = direction.xyz().normalize();
    let (fp, obj_color, _num_iter) = sphere_march(direction, origin, &sdf.stream);

    if fp < MAX_DISTANCE {
        //The location at which we have found something
        let eval_location = origin + (direction * fp);
        let n = calc_normal(&sdf.stream, eval_location);

        let ll: Vec3 = LIGHT_LOCATION.into();
        //Trace ray to light location
        let shadow_ray_direction = (ll - eval_location).normalize();
        let phong = shadow_ray_direction.dot(n).max(0.0).min(1.0);

        /*
            let (lightray, num_skips) = shadow_march(
            eval_location + (n * 1.0),
            LIGHT_LOCATION.into(),
            SHADOW_SHARPNESS,
            &code.stream,
        );

            num_iter += num_skips;
             */
        let lightness = Vec3::from(LIGHT_COLOR) * 1.0; //lightray;

        let mut light: Vec3 =
            (phong * lightness + (1.0 - phong) * Vec3::from(AMBIENT_LIGHT)) * obj_color;

        light.x = light.x.max(0.0).min(1.0);
        light.y = light.y.max(0.0).min(1.0);
        light.z = light.z.max(0.0).min(1.0);

        Rgba([light.x, light.y, light.z, 1.0])
    } else {
        //Did not intersect
        Rgba([0.0, 0.0, 0.0, 0.0])
    }
}

///Calculates the unlit pixel for a 2d sdf
pub fn unlit_2d(
    (img_width, img_height): (usize, usize),
    (x, y): (usize, usize),
    camera: Camera2d,
    sdf: Arc<PrimaryStream2d>,
) -> Rgba<f32> {
    let coord = camera.pixel_to_worldspace(x, y, img_width, img_height);
    let result = eval_2d_from_code(coord, &sdf.stream);

    if result.result < 0.0 {
        Rgba([result.color.x, result.color.y, result.color.z, 1.0])
    } else {
        Rgba([0.0; 4])
    }
}
