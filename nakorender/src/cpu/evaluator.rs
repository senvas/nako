/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::usize;

use nako::{
    glam::{Mat3, Mat4, Vec2, Vec3, Vec3Swizzles, Vec4Swizzles},
    operations::{
        planar::primitives2d::{bezier2d, dot2_vec3, sign},
        volumetric::{BoxExact, Plane, Sphere},
    },
    sdf::{BBox, NakoResult},
    serialize::{OpTy, Word},
};

#[inline]
fn as_f32(s: u32) -> f32 {
    f32::from_be_bytes(s.to_be_bytes())
}

fn pop_vec2(code: &[Word], offset: usize) -> Vec2 {
    Vec2::new(as_f32(code[offset + 0]), as_f32(code[offset + 1]))
}

fn pop_vec3(code: &[Word], offset: usize) -> Vec3 {
    Vec3::new(
        as_f32(code[offset + 0]),
        as_f32(code[offset + 1]),
        as_f32(code[offset + 2]),
    )
}

fn pop_mat4(code: &[Word], offset: usize) -> Mat4 {
    let mut array = [0.0; 16];
    for (idx, val) in array.iter_mut().enumerate() {
        *val = as_f32(code[offset + idx]);
    }

    Mat4::from_cols_array(&array)
}

pub(crate) fn eval_from_code(coord: Vec3, code: &[Word]) -> NakoResult {
    let mut ptr = 0;
    let mut coord = coord;
    let mut primary = NakoResult::default();
    let mut secondary = NakoResult::default();

    while ptr < code.len() {
        let op: OpTy = code[ptr].into();
        //println!("op: {:?}", op);
        match op {
            OpTy::Skip => {
                let bound = BBox {
                    min: Vec3::new(
                        as_f32(code[ptr + 1]),
                        as_f32(code[ptr + 2]),
                        as_f32(code[ptr + 3]),
                    ),
                    max: Vec3::new(
                        as_f32(code[ptr + 4]),
                        as_f32(code[ptr + 5]),
                        as_f32(code[ptr + 6]),
                    ),
                };

                let skip_to = code[ptr + 7];

                ptr += 8;
                //We can safely skip when the the current primary result is nearer then the whole "to be skipped" bound
                if !bound.is_sphere_intersecting(coord, primary.result) {
                    //println!("@{} skipping to {}", ptr, ptr + skip_to as usize);
                    ptr = ptr + skip_to as usize;
                    //*num_skips += skip_to as usize;
                    //println!("Skipped {}", num_skips);
                }
            }
            //Combinators
            OpTy::Union => {
                primary = primary.union(&secondary);

                //Reset secondary
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothUnion => {
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_union(&secondary, smoothness);
                //Reset secondary
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Intersection => {
                primary = primary.intersection(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothIntersection => {
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_intersection(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Subtraction => {
                primary = primary.subtraction(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothSubtraction => {
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_subtraction(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }
            //Volumetric effects
            OpTy::Round => {
                secondary.result -= as_f32(code[ptr + 1]);
                ptr += 2;
            }
            OpTy::Onion => {
                secondary.result = secondary.result.abs() - as_f32(code[ptr + 1]);
                ptr += 2;
            }

            //2d -> 3d Projections
            OpTy::PlanarProjection => {
                //Currently just projecting xy, TODO find actual worldspace mapping based on UV
                let coord2d = coord.xz();

                let code_len = code[ptr + 1] as usize;
                //println!("Code: {:?}", &code[ptr+2 .. ptr+2+code_len]);
                let res2d = eval_2d_from_code(coord2d, &code[ptr + 2..ptr + 2 + code_len]);

                //If the projection point is inside the form, return just the length, otherwise the distance to the next edge
                if res2d.result >= 0.0 {
                    //Calculate distance relative to the intersection distance
                    let on_plane_coord = Vec3::new(coord.x, 0.0, coord.z) + res2d.result * Vec3::X;
                    secondary.result = (coord - on_plane_coord).length();
                } else {
                    secondary.result = coord.y.abs();
                }
                secondary.color = res2d.color;

                ptr += 2 + code_len;
            }
            OpTy::Revolution => {
                let offset = as_f32(code[ptr + 1]);
                let codelen = code[ptr + 2] as usize;
                //Move into 2d space
                let q = Vec2::new(coord.xz().length() - offset, coord.y);
                secondary = eval_2d_from_code(q, &code[ptr + 3..ptr + 3 + codelen]);

                ptr += 3 + codelen;
            }
            OpTy::Extrude => {
                let h = as_f32(code[ptr + 1]);
                let codelen = code[ptr + 2] as usize;
                let newres = eval_2d_from_code(coord.xy(), &code[ptr + 3..ptr + 3 + codelen]);
                let w = Vec2::new(newres.result, coord.z.abs() - h);
                secondary.result = w.max_element().min(0.0) + w.max(Vec2::ZERO).length();
                secondary.color = newres.color;

                ptr += 3 + codelen;
            }
            //Coord manipulation
            OpTy::CoordAdd => {
                let to_add = Vec3::new(
                    as_f32(code[ptr + 1]),
                    as_f32(code[ptr + 2]),
                    as_f32(code[ptr + 3]),
                );

                coord += to_add;
                ptr += 4;
            }
            OpTy::CoordMul => {
                let to_mul = Vec3::new(
                    as_f32(code[ptr + 1]),
                    as_f32(code[ptr + 2]),
                    as_f32(code[ptr + 3]),
                );

                coord *= to_mul;
                ptr += 4;
            }
            OpTy::CoordMatMul => {
                let matrix = pop_mat4(code, ptr + 1);
                let new = matrix * coord.extend(1.0);
                coord = new.xyz() / new.w;
                ptr += 17;
            }

            //Result Modifiers
            OpTy::Color => {
                let color = Vec3::new(
                    as_f32(code[ptr + 1]),
                    as_f32(code[ptr + 2]),
                    as_f32(code[ptr + 3]),
                );
                secondary.color = color;
                ptr += 4;
            }

            //Primitives
            OpTy::Sphere => {
                secondary.result = Sphere {
                    radius: as_f32(code[ptr + 1]),
                }
                .evaluate(coord);
                ptr += 2;
            }
            OpTy::Plane => {
                secondary.result = Plane {
                    normal: Vec3::new(
                        as_f32(code[ptr + 1]),
                        as_f32(code[ptr + 2]),
                        as_f32(code[ptr + 3]),
                    ),
                    height: as_f32(code[ptr + 4]),
                }
                .evaluate(coord);
                ptr += 5;
            }
            OpTy::Box => {
                secondary.result = BoxExact {
                    extent: Vec3::new(
                        as_f32(code[ptr + 1]),
                        as_f32(code[ptr + 2]),
                        as_f32(code[ptr + 3]),
                    ),
                }
                .evaluate(coord);

                ptr += 4;
            }
            OpTy::Line => {
                let a = pop_vec3(code, ptr + 1);
                let b = pop_vec3(code, ptr + 4);
                let width = as_f32(code[ptr + 7]);

                let pa = coord - a;
                let ba = b - a;
                let h = (pa.dot(ba) / ba.dot(ba)).clamp(0.0, 1.0);
                secondary.result = (pa - ba * h).length() - width;

                ptr += 8;
            }
            OpTy::Triangle => {
                let thickness = as_f32(code[ptr + 1]);
                let verts = [
                    pop_vec3(code, ptr + 2),
                    pop_vec3(code, ptr + 5),
                    pop_vec3(code, ptr + 8),
                ];

                //Find edges
                let ba = verts[1] - verts[0];
                let pa = coord - verts[0];

                let cb = verts[2] - verts[1];
                let pb = coord - verts[1];

                let ac = verts[0] - verts[2];
                let pc = coord - verts[2];

                let norm = ba.cross(ac);

                secondary.result = (if sign(ba.cross(norm).dot(pa))
                    + sign(cb.cross(norm).dot(pb))
                    + sign(ac.cross(norm).dot(pc))
                    < 2.0
                {
                    dot2_vec3(ba * (ba.dot(pa) / dot2_vec3(ba)).clamp(0.0, 1.0) - pa)
                        .min(dot2_vec3(
                            cb * (cb.dot(pb) / dot2_vec3(cb)).clamp(0.0, 1.0) - pb,
                        ))
                        .min(dot2_vec3(
                            ac * (ac.dot(pc) / dot2_vec3(ac)).clamp(0.0, 1.0) - pc,
                        ))
                } else {
                    norm.dot(pa) * norm.dot(pa) / dot2_vec3(norm)
                })
                .sqrt()
                    - thickness;

                ptr += 11;
            }
            _ => {
                panic!("Invalid op: {} @ {}", code[ptr], ptr);
            }
        }
    }

    primary
}

pub(crate) fn eval_2d_from_code(coord: Vec2, code: &[Word]) -> NakoResult {
    let mut primary = NakoResult::default();
    let mut secondary = NakoResult::default();
    let mut ptr = 0;
    let mut coord = coord;

    while ptr < code.len() {
        let op: OpTy = OpTy::from(code[ptr]);
        match op {
            //NoSkip for now
            //Combinators
            OpTy::Union => {
                primary = primary.union(&secondary);

                //Reset secondary
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothUnion => {
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_union(&secondary, smoothness);
                //Reset secondary
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Intersection => {
                primary = primary.intersection(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothIntersection => {
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_intersection(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Subtraction => {
                primary = primary.subtraction(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothSubtraction => {
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_subtraction(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }

            OpTy::Round => {
                secondary.result -= as_f32(code[ptr + 1]);
                ptr += 2;
            }
            OpTy::Onion => {
                secondary.result = secondary.result.abs() - as_f32(code[ptr + 1]);
                ptr += 2;
            }

            //Coord manipulation
            OpTy::CoordAdd => {
                let to_add = Vec2::new(as_f32(code[ptr + 1]), as_f32(code[ptr + 2]));
                coord += to_add;
                ptr += 3;
            }
            OpTy::CoordMul => {
                let to_mul = Vec2::new(as_f32(code[ptr + 1]), as_f32(code[ptr + 2]));

                coord *= to_mul;
                ptr += 3;
            }
            OpTy::CoordMatMul => {
                let matrix = Mat3::from_cols(
                    pop_vec3(code, ptr + 1),
                    pop_vec3(code, ptr + 4),
                    pop_vec3(code, ptr + 7),
                );

                coord = matrix.transform_point2(coord);
                ptr += 10;
            }
            //Result modifier
            OpTy::Color => {
                let color = Vec3::new(
                    as_f32(code[ptr + 1]),
                    as_f32(code[ptr + 2]),
                    as_f32(code[ptr + 3]),
                );
                secondary.color = color;
                ptr += 4;
            }
            //Primitives
            OpTy::Circle => {
                secondary.result = coord.length() - as_f32(code[ptr + 1]);
                ptr += 2;
            }
            OpTy::Box2d => {
                let d = coord.abs() - Vec2::new(as_f32(code[ptr + 1]), as_f32(code[ptr + 2]));
                secondary.result = d.max(Vec2::ZERO).length() + (d.max_element()).min(0.0);
                ptr += 3;
            }
            OpTy::Line2d => {
                let a = pop_vec2(code, ptr + 1);
                let b = pop_vec2(code, ptr + 3);
                let width = as_f32(code[ptr + 5]);

                let pa = coord - a;
                let ba = b - a;
                let h = (pa.dot(ba) / ba.dot(ba)).clamp(0.0, 1.0);
                let d = (pa - ba * h).length();

                secondary.result = d - width;

                ptr += 6;
            }
            OpTy::Polygon2d => {
                let num_points = code[ptr + 1] as usize;
                let offset = ptr + 2;

                let firstv = pop_vec2(code, offset);
                let mut d = (coord - firstv).dot(coord - firstv);
                let mut s = 1.0;

                let mut prev_vec = pop_vec2(code, offset + ((num_points - 1) * 2));
                let mut this_vec;
                for i in 0..num_points {
                    this_vec = pop_vec2(code, offset + (i * 2));
                    //distance
                    let e = prev_vec - this_vec;
                    let w = coord - this_vec;
                    let b = w - e * (w.dot(e) / e.dot(e)).clamp(0.0, 1.0);
                    d = d.min(b.dot(b));

                    //Based on winding number, might change sign
                    match (
                        coord.y >= this_vec.y,
                        coord.y < prev_vec.y,
                        e.x * w.y > e.y * w.x,
                    ) {
                        (true, true, true) | (false, false, false) => s *= -1.0,
                        _ => {}
                    }

                    prev_vec = this_vec;
                }

                secondary.result = s * d.sqrt();
                ptr += 2 + num_points * 2;
            }
            OpTy::Bezier2d => {
                let start = pop_vec2(code, ptr + 1);
                let ctrl = pop_vec2(code, ptr + 3);
                let end = pop_vec2(code, ptr + 5);
                let width = as_f32(code[ptr + 7]);

                secondary.result = bezier2d(coord, start, ctrl, end, width);

                ptr += 8;
            }
            _ => panic!("Unknow 2d op: {:?}:{}!", op, code[ptr]),
        }
    }

    primary
}
