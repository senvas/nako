/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::{mem::size_of, sync::Arc};

use marp::{
    buffer::{Buffer, BufferUsage, SharingMode, UploadStrategy},
    command_buffer::CommandBuffer,
    device::{Device, Queue},
    memory::MemoryUsage,
};
use nako::{
    serialize::Word,
    stream::{PrimaryStream, PrimaryStream2d},
};

use crate::marp::Queues;

///Manages the current sdf buffers.
pub(crate) struct SdfBufferManager {
    ///Code thats scheduled to be uploaded. Data is already aligned in a way that the
    ///data contains a header with context info about the code as well as the code itself
    #[allow(dead_code)]
    upload_code: Option<Vec<u8>>,

    ///Contains the currently used code thats being executed
    pub code: Arc<Buffer>,
    //Upload queue, currently same as everywhere, but maybe that will be a transfer queue at some point?
    #[allow(dead_code)]
    queue: Arc<Queue>,
    #[allow(dead_code)]
    device: Arc<Device>,
}

impl SdfBufferManager {
    pub fn new(device: Arc<Device>, queues: &Queues) -> Self {
        //panic!("Marp backend not yet implemented !");

        let queue = queues.graphics_queue.clone();
        //Upload a dummy buffer and memory for now which will get updated at some point
        let code = Buffer::new(
            device.clone(),
            1 as u64,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create inital code buffer");
        let code_upload = code.copy(queue.clone(), vec![u8::MAX]).unwrap();
        code_upload.wait(u64::MAX).unwrap();

        SdfBufferManager {
            upload_code: None,

            code,

            queue,
            device,
        }
    }

    pub fn update_code(&mut self, new_code: PrimaryStream) {
        self.upload_stream(new_code.stream);
    }

    pub fn update_code_2d(&mut self, new_code: PrimaryStream2d) {
        self.upload_stream(new_code.stream);
    }

    fn upload_stream(&mut self, stream: Vec<Word>) {
        //Upload inlined priamry stream which should already be in perfect layout
        let new_buffer = Buffer::new(
            self.device.clone(),
            (stream.len() * size_of::<Word>()) as u64,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create inital mem buffer");
        //Upload actual data
        new_buffer
            .copy(self.queue.clone(), stream)
            .expect("Failed to upload new code")
            .wait(u64::MAX)
            .unwrap();
        self.code = new_buffer;
    }

    ///If any updates are pending, schedule them on this command buffer.
    #[allow(dead_code)]
    pub fn schedule_updates(&mut self, command_buffer: Arc<CommandBuffer>) -> Arc<CommandBuffer> {
        println!("Scheduling not yet implemented");
        command_buffer
    }
}
