/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

//use spirv_builder::SpirvBuilder;
//use spirv_builder::SpirvBuilder;

///creates a constant buffer `shader_name` that includes the shader bytecode from a shader called `shader_file_name`.
///for instance
///`include_shader!(MY_COMPUTE_SHADER, "compute_shader.spv");`
#[macro_export]
macro_rules! include_shader {
    ($shader_file_name:expr) => {{
        let bytes = include_bytes!(concat!(
            "../../../resources/shader/glsl/",
            $shader_file_name
        ));
        marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..]))
            .expect("Failed to read shader file")
    }};
}
