/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::sync::Arc;

use marp::{
    ash::vk::{
        AccessFlags, DependencyFlags, DescriptorPoolSize, DescriptorType, Extent2D, Format,
        ImageLayout, PipelineBindPoint, PipelineStageFlags, ShaderStageFlags,
    },
    buffer::{Buffer, SharingMode},
    command_buffer::CommandBuffer,
    descriptor::{DescResource, DescriptorPool, DescriptorSet, PushConstant, StdDescriptorPool},
    device::Device,
    image::{AbstractImage, Image, ImageInfo, ImageType, ImageUsage, MipLevel},
    memory::MemoryUsage,
    pipeline::{ComputePipeline, PipelineLayout},
    shader::{AbstractShaderModule, ShaderModule, Stage},
};
use shared::GpuCamera2d;

use super::pass::Pass;
use crate::{
    camera::Camera2d,
    include_shader,
    marp::{passes::dispatch_size, resources::sdf_buffer::SdfBufferManager, Queues},
};

pub(crate) struct PPass2dData {
    pub(crate) albedo_depth: Arc<Image>,
    descriptor_set: Arc<DescriptorSet>,
    is_transitioned: bool,
}

pub(crate) struct PrimaryPass2d {
    pub(crate) data: Vec<PPass2dData>,
    pipeline: Arc<ComputePipeline>,
    camera: PushConstant<GpuCamera2d>,
}

impl PrimaryPass2d {
    pub fn new(
        buffers: &SdfBufferManager,
        device: Arc<Device>,
        extent: Extent2D,
        num_slots: usize,
    ) -> Self {
        let descriptor_pool = StdDescriptorPool::new(
            device.clone(),
            vec![
                DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(num_slots as u32 * 1) //albedo_depth
                    .build(),
                DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(num_slots as u32 * 1) //code
                    .build(),
            ]
            .as_slice(),
            num_slots as u32,
        )
        .unwrap();

        let mut data = Vec::with_capacity(num_slots);
        for _i in 0..num_slots {
            let albedo_depth = Image::new(
                device.clone(),
                ImageInfo::new(
                    ImageType::Image2D {
                        width: extent.width,
                        height: extent.height,
                        samples: 1,
                    },
                    Format::R32G32B32A32_SFLOAT,
                    None,
                    Some(MipLevel::Specific(1)),
                    ImageUsage {
                        transfer_src: true,
                        storage: true,
                        color_aspect: true,
                        ..Default::default()
                    },
                    MemoryUsage::GpuOnly,
                    None,
                ),
                SharingMode::Exclusive,
            )
            .unwrap();

            //Allocate the descriptor set
            let mut descriptor_set = descriptor_pool.next();

            descriptor_set
                .add(DescResource::new_buffer(
                    0,
                    vec![buffers.code.clone()],
                    DescriptorType::STORAGE_BUFFER,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    1,
                    vec![(albedo_depth.clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            let descriptor_set = descriptor_set.build().unwrap();

            data.push(PPass2dData {
                albedo_depth,
                descriptor_set,
                is_transitioned: false,
            });
        }

        //Create initial push constant
        let camera_const = PushConstant::new(
            GpuCamera2d::from(Camera2d::default()),
            ShaderStageFlags::COMPUTE,
        );

        //Setup the compute pipeline.
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*camera_const.range()],
        )
        .unwrap();

        let shader =
            ShaderModule::new_from_code(device.clone(), include_shader!("primary_2d_pass.spv"))
                .unwrap()
                .to_stage(Stage::Compute, "main");

        let pipeline = ComputePipeline::new(device.clone(), shader, pipe_layout).unwrap();

        PrimaryPass2d {
            data,
            pipeline,
            camera: camera_const,
        }
    }

    pub fn update_camera(&mut self, new_camera: GpuCamera2d) {
        *self.camera.get_content_mut() = new_camera;
    }

    pub fn update_buffer(&mut self, idx: usize, buffer: Arc<Buffer>) {
        //Update all buffers
        //TODO only update those that are actually changed. For instance the stack wont change that often
        self.data[idx]
            .descriptor_set
            .update(DescResource::new_buffer(
                0,
                vec![buffer],
                DescriptorType::STORAGE_BUFFER,
            ))
            .unwrap();
    }
}

impl Pass for PrimaryPass2d {
    fn pre(
        &mut self,
        queues: &Queues,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        //Transition every resource that isn't yet in the correct layout
        if self.data[slot_index].is_transitioned {
            return command_buffer;
        }

        //We want our images to be in the general layout, so we can write to them in the primary pass
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::ALL_COMMANDS,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![self.data[slot_index].albedo_depth.new_image_barrier(
                    Some(ImageLayout::UNDEFINED),
                    Some(ImageLayout::GENERAL),
                    Some(queues.graphics_queue.clone()), //Keep being on graphics queue for now.
                    Some(queues.graphics_queue.clone()),
                    None,
                    Some(AccessFlags::SHADER_WRITE),
                    None,
                )],
            )
            .expect("Failed to transition image in primary pass");
        //Mark this slot as transitioned
        self.data[slot_index].is_transitioned = true;
        command_buffer
    }

    fn record(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        //Bind descriptorset
        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[slot_index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind Primary pass Descriptorset");

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind primary pipeline");
        //upload current camera
        command_buffer
            .cmd_push_constants(self.pipeline.layout(), &self.camera)
            .unwrap();
        command_buffer
            .cmd_dispatch(dispatch_size(&self.data[slot_index].albedo_depth))
            .expect("Failed to schedule primary pass");

        command_buffer
    }
}
