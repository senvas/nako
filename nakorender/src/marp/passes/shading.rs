/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::sync::Arc;

use marp::{
    ash::vk::{
        AccessFlags, DependencyFlags, DescriptorPoolSize, DescriptorType, Extent2D, Format,
        ImageLayout, PipelineBindPoint, PipelineStageFlags, ShaderStageFlags,
    },
    buffer::SharingMode,
    command_buffer::CommandBuffer,
    descriptor::{DescResource, DescriptorPool, DescriptorSet, PushConstant, StdDescriptorPool},
    device::Device,
    image::{AbstractImage, Image, ImageInfo, ImageType, ImageUsage, MipLevel},
    memory::MemoryUsage,
    pipeline::{ComputePipeline, PipelineLayout},
    shader::{AbstractShaderModule, ShaderModule, Stage},
};

use super::{direct_light::DirectLightPass, pass::Pass, pass_primary::PrimaryPass};
use crate::{
    include_shader,
    marp::{passes::dispatch_size, Queues},
};

#[repr(C)]
pub(crate) struct ShadingParameter {
    gamma: f32,
    pad0: [f32; 3],
}

pub(crate) struct ShadingPassData {
    pub(crate) shaded: Arc<Image>,
    descriptor_set: Arc<DescriptorSet>,
    is_transitioned: bool,
}

pub(crate) struct ShadingPass {
    pub(crate) data: Vec<ShadingPassData>,
    pipeline: Arc<ComputePipeline>,
    shading_const: PushConstant<ShadingParameter>,
}

impl ShadingPass {
    pub fn new(
        device: Arc<Device>,
        extent: Extent2D,
        num_slots: usize,
        primary_pass: &PrimaryPass,
        direct_light_pass: &DirectLightPass,
    ) -> Self {
        let descriptor_pool = StdDescriptorPool::new(
            device.clone(),
            vec![DescriptorPoolSize::builder()
                .ty(DescriptorType::STORAGE_IMAGE)
                //In: Albedo, Direct Light, Out: shaded
                .descriptor_count(num_slots as u32 * 3)
                .build()]
            .as_slice(),
            num_slots as u32,
        )
        .unwrap();

        let mut data = Vec::with_capacity(num_slots);
        for i in 0..num_slots {
            let shaded = Image::new(
                device.clone(),
                ImageInfo::new(
                    ImageType::Image2D {
                        width: extent.width,
                        height: extent.height,
                        samples: 1,
                    },
                    Format::R32G32B32A32_SFLOAT, //Unused Alpha atm
                    None,
                    Some(MipLevel::Specific(1)),
                    ImageUsage {
                        transfer_src: true,
                        storage: true,
                        color_aspect: true,
                        ..Default::default()
                    },
                    MemoryUsage::GpuOnly,
                    None,
                ),
                SharingMode::Exclusive,
            )
            .unwrap();

            //Allocate the descriptor set
            let mut descriptor_set = descriptor_pool.next();

            descriptor_set
                .add(DescResource::new_image(
                    0,
                    vec![(
                        primary_pass.data[i].albedo.clone(),
                        None,
                        ImageLayout::GENERAL,
                    )],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    1,
                    vec![(
                        direct_light_pass.data[i].direct_light.clone(),
                        None,
                        ImageLayout::GENERAL,
                    )],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    2,
                    vec![(shaded.clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            let descriptor_set = descriptor_set.build().unwrap();

            data.push(ShadingPassData {
                shaded,
                descriptor_set,
                is_transitioned: false,
            });
        }

        //Create initial push constant
        let shading_const = PushConstant::new(
            ShadingParameter {
                gamma: 2.2,
                pad0: [0.0; 3],
            },
            ShaderStageFlags::COMPUTE,
        );

        //Setup the compute pipeline.
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*shading_const.range()],
        )
        .unwrap();

        let shader = ShaderModule::new_from_code(device.clone(), include_shader!("shading.spv"))
            .unwrap()
            .to_stage(Stage::Compute, "main");

        let pipeline = ComputePipeline::new(device.clone(), shader, pipe_layout).unwrap();

        ShadingPass {
            data,
            pipeline,
            shading_const,
        }
    }

    #[allow(dead_code)]
    pub fn update_shading_const(&mut self, new: ShadingParameter) {
        //update image widht/height
        *self.shading_const.get_content_mut() = new;
    }
}

impl Pass for ShadingPass {
    fn pre(
        &mut self,
        queues: &Queues,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        //Transition every resource that isn't yet in the correct layout
        if self.data[slot_index].is_transitioned {
            return command_buffer;
        }

        //We want our images to be in the general layout, so we can write to them in the primary pass
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::ALL_COMMANDS,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![self.data[slot_index].shaded.new_image_barrier(
                    Some(ImageLayout::UNDEFINED),
                    Some(ImageLayout::GENERAL),
                    Some(queues.graphics_queue.clone()), //Keep being on graphics queue for now.
                    Some(queues.graphics_queue.clone()),
                    None,
                    Some(AccessFlags::SHADER_WRITE),
                    None,
                )],
            )
            .expect("Failed to transition image in direct light pass");
        //Mark this slot as transitioned
        self.data[slot_index].is_transitioned = true;
        command_buffer
    }
    fn record(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        //Bind descriptorset
        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[slot_index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind ShadingPass Descriptorset");

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind shading pipeline");
        //upload current camera
        command_buffer
            .cmd_push_constants(self.pipeline.layout(), &self.shading_const)
            .unwrap();

        command_buffer
            .cmd_dispatch(dispatch_size(&self.data[slot_index].shaded))
            .expect("Failed to schedule shading pass");

        command_buffer
    }
}
