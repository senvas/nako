/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::sync::Arc;

use marp::command_buffer::CommandBuffer;

use crate::marp::Queues;

///Some pass of any kind. Could be a render, compute or transfer pass.
pub(crate) trait Pass {
    ///Stuff that needs to be executed before this pass is recorded. For instance if work from a previous frame needs to be synced.
    fn pre(
        &mut self,
        _queues: &Queues,
        command_buffer: Arc<CommandBuffer>,
        _slot: usize,
    ) -> Arc<CommandBuffer> {
        command_buffer
    }
    ///Stuff that needs to be executed after this pass is recorded. For instance if resources need to be synchronised
    ///before they can be accessed by a following frame.
    fn post(
        &mut self,
        _queues: &Queues,
        command_buffer: Arc<CommandBuffer>,
        _slot: usize,
    ) -> Arc<CommandBuffer> {
        command_buffer
    }
    fn record(&mut self, command_buffer: Arc<CommandBuffer>, slot: usize) -> Arc<CommandBuffer>;
}
