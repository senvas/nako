/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::{collections::HashMap, sync::Arc};

use marp::{ash::vk::Extent2D, device::Device};
use nako::stream::{PrimaryStream, PrimaryStream2d};

use crate::{
    backend::{LayerId, LayerId2d, LayerId3d, LayerInfo},
    camera::{Camera, Camera2d},
};

use super::{
    layer_combiner::CombineInfo,
    layer_renderer::{lit3d::Lit3d, unlit2d::Unlit2d, LayerRenderer},
    Queues,
};

///Layer which takes an Stream `S` and a camera type `C` to display anything.
struct Layer<S, C> {
    info: LayerInfo,
    renderer: Box<dyn LayerRenderer<Camera = C, Stream = S>>,
}

enum LayerType {
    L2d(Layer<PrimaryStream2d, Camera2d>),
    L3d(Layer<PrimaryStream, Camera>),
}

impl LayerType {
    fn new_lit_3d(device: Arc<Device>, queues: Queues, num_slots: usize) -> Self {
        LayerType::L3d(Layer {
            info: LayerInfo {
                extent: (1, 1),
                location: (0, 0),
            },
            renderer: Box::new(Lit3d::new(device, queues, num_slots)),
        })
    }

    fn new_unlit_2d(device: Arc<Device>, queues: Queues, num_slots: usize) -> Self {
        LayerType::L2d(Layer {
            info: LayerInfo {
                extent: (1, 1),
                location: (0, 0),
            },
            renderer: Box::new(Unlit2d::new(device, queues, num_slots)),
        })
    }

    fn update_info(&mut self, new_info: LayerInfo) {
        let ext = Extent2D {
            width: new_info.extent.0 as u32,
            height: new_info.extent.1 as u32,
        };
        match self {
            LayerType::L2d(l) => {
                l.info = new_info;
                l.renderer.update_extent(ext);
            }
            LayerType::L3d(l) => {
                l.info = new_info;
                l.renderer.update_extent(ext);
            }
        }
    }

    fn update_sdf_2d(&mut self, sdf: PrimaryStream2d) {
        if let LayerType::L2d(l) = self {
            l.renderer.update_sdf(sdf);
        }
    }

    fn update_sdf(&mut self, sdf: PrimaryStream) {
        if let LayerType::L3d(l) = self {
            l.renderer.update_sdf(sdf);
        }
    }

    fn update_camera_2d(&mut self, camera: Camera2d) {
        if let LayerType::L2d(l) = self {
            l.renderer.update_camera(camera);
        }
    }

    fn update_camera(&mut self, camera: Camera) {
        if let LayerType::L3d(l) = self {
            l.renderer.update_camera(camera);
        }
    }

    fn update_num_slots(&mut self, new_number: usize) {
        match self {
            LayerType::L2d(l) => {
                l.renderer.update_number_of_slots(new_number);
            }
            LayerType::L3d(l) => {
                l.renderer.update_number_of_slots(new_number);
            }
        }
    }

    fn record(&mut self, slot: usize) -> CombineInfo {
        let (layer_info, mut combine_info) = match self {
            LayerType::L2d(l) => (l.info, l.renderer.record(slot)),
            LayerType::L3d(l) => (l.info, l.renderer.record(slot)),
        };

        //Update layer info with correct origin and the minima of the provided extent
        //and the intended extent
        match &mut combine_info {
            CombineInfo::Ready { image: _, info } => {
                info.location = layer_info.location;
            }
            CombineInfo::Rendering {
                image: _,
                info,
                sem: _,
                stage: _,
            } => {
                info.location = layer_info.location;
            }
        }

        combine_info
    }
}

pub struct LayerManager {
    layers: HashMap<LayerId, LayerType>,
    order: Vec<LayerId>,

    //vulkan context
    device: Arc<Device>,
    queues: Queues,
    num_slots: usize,
}

impl LayerManager {
    pub fn new(device: Arc<Device>, queues: Queues, num_slots: usize) -> Self {
        LayerManager {
            layers: HashMap::new(),
            order: vec![],

            device,
            queues,
            num_slots,
        }
    }

    pub fn alloc_layer_2d(&mut self) -> LayerId2d {
        let mut idx = 0;
        let mut id = LayerId::Planar(LayerId2d(idx));
        while self.layers.contains_key(&id) {
            idx += 1;
            id = LayerId::Planar(LayerId2d(idx));
        }

        self.layers.insert(
            id,
            LayerType::new_unlit_2d(self.device.clone(), self.queues.clone(), self.num_slots),
        );
        LayerId2d(idx)
    }

    pub fn alloc_layer_3d(&mut self) -> LayerId3d {
        let mut idx = 0;
        let mut id = LayerId::Volumetric(LayerId3d(idx));
        while self.layers.contains_key(&id) {
            idx += 1;
            id = LayerId::Volumetric(LayerId3d(idx));
        }

        self.layers.insert(
            id,
            LayerType::new_lit_3d(self.device.clone(), self.queues.clone(), self.num_slots),
        );

        LayerId3d(idx)
    }

    pub fn set_num_slots(&mut self, num_slots: usize) {
        if num_slots == self.num_slots {
            return;
        }

        self.num_slots = num_slots;
        //Have to recreate all layers.
        //TODO only recreate used ones and tag unused for recreation if they get used again
        for (_k, l) in &mut self.layers {
            l.update_num_slots(self.num_slots);
        }
    }

    pub fn set_info(&mut self, id: LayerId, new_info: LayerInfo) {
        if let Some(l) = self.layers.get_mut(&id) {
            l.update_info(new_info);
        }
    }

    pub fn set_order(&mut self, new_order: &[LayerId]) {
        #[cfg(Debug)]
        {
            println!("Checking layer ids");
            for new_id in &new_order {
                assert!(
                    self.layers.contains_key(new_id),
                    "New order contains unknown id"
                );
            }
        }

        self.order = new_order.to_vec();
    }

    pub fn update_camera_2d(&mut self, id: LayerId2d, camera: Camera2d) {
        if let Some(l) = self.layers.get_mut(&id.into()) {
            l.update_camera_2d(camera);
        }
    }

    pub fn update_camera(&mut self, id: LayerId3d, camera: Camera) {
        if let Some(l) = self.layers.get_mut(&id.into()) {
            l.update_camera(camera);
        }
    }

    pub fn update_sdf_2d(&mut self, id: LayerId2d, sdf: PrimaryStream2d) {
        if let Some(l) = self.layers.get_mut(&id.into()) {
            l.update_sdf_2d(sdf);
        }
    }

    pub fn update_sdf(&mut self, id: LayerId3d, sdf: PrimaryStream) {
        if let Some(l) = self.layers.get_mut(&id.into()) {
            l.update_sdf(sdf);
        }
    }

    pub fn record(&mut self, slot: usize) -> Vec<CombineInfo> {
        //Collect in order
        self.order
            .clone()
            .iter()
            .map(|layer_id| self.layers.get_mut(layer_id).unwrap().record(slot))
            .collect()
    }
}
