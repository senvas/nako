/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
//! #Marp backend
//!
//! TODO describes what makes this different to the other apis

use std::sync::Arc;

use crate::{
    backend::{Backend, LayerId, LayerId2d, LayerId3d, LayerInfo},
    camera::{Camera, Camera2d},
};

use marp::{
    ash::vk::{Extent2D, PhysicalDeviceVulkan12Features, PresentModeKHR},
    device::{Device, DeviceBuilder, PhysicalDevice, Queue},
    image::ImageUsage,
    instance::Instance,
    miscellaneous::{AppInfo, Debug, DeviceExtension, InstanceExtensions, InstanceLayer, Version},
    swapchain::{surface::Surface, Swapchain},
};
use marp_surface_winit::{
    winit::{event_loop::EventLoop, platform::unix::EventLoopWindowTargetExtUnix, window::Window},
    WinitSurface,
};
use nako::stream::{PrimaryStream, PrimaryStream2d};

pub(crate) mod passes;
pub(crate) mod resources;

pub(crate) mod layer_combiner;
pub(crate) mod layer_renderer;
pub(crate) mod layering;

mod utils;
pub use utils::*;

use self::{layer_combiner::LayerCombiner, layering::LayerManager};

#[derive(Clone)]
pub struct Queues {
    graphics_queue: Arc<Queue>,
}

pub struct MarpBackend {
    device: Arc<Device>,
    queues: Queues,
    swapchain: Arc<Swapchain>,
    //The extent we created the frames for.
    frame_extent: Extent2D,
    //next to render frame index
    frame_idx: usize,

    //handles combination of several layers into one image
    layer_combiner: LayerCombiner,
    layers: LayerManager,
}

impl MarpBackend {
    fn resize(&mut self, new_extent: Extent2D) {
        //Sometime we have to resize. We do that by reading the new extent information
        //And setting up all subsystems based on it. While there are more effective ways to do that,
        //its nice and easy for now.

        self.device.wait_idle().unwrap();

        println!(
            "Old ext was {:?}, new is {:?}",
            self.frame_extent, new_extent
        );
        //TODO handle dpi scaling and this stuff?

        //Recreate swapchain and transform images into present layout.
        self.swapchain.recreate(new_extent);
        let submit_fence = self
            .swapchain
            .images_to_present_layout(self.queues.graphics_queue.clone());
        submit_fence.wait(u64::MAX).unwrap();

        //Notify layer manager of new swapchain count
        self.layers.set_num_slots(self.swapchain.get_images().len());

        //Build new layer combiner
        self.layer_combiner = LayerCombiner::new(
            self.swapchain.get_images(),
            self.device.clone(),
            &self.queues,
        );
        //Update stack size to new frame size
        self.frame_extent = new_extent;
    }
}

#[cfg(all(unix, not(target_os = "android")))]
pub fn is_wayland(events_loop: &EventLoop<()>) -> bool {
    events_loop.is_wayland()
}

#[cfg(windows)]
pub fn is_wayland(events_loop: &winit::event_loop::EventLoop<()>) -> bool {
    false
}

impl Backend for MarpBackend {
    fn new(window: &Window, event_loop: &EventLoop<()>) -> Self {
        //Basic vulkan setup routine
        let app_info = AppInfo::new(
            "NakoTest".to_string(),
            Version::new(0, 1, 0),
            "Nako".to_string(),
            Version::new(0, 1, 0),
            Version::new(1, 2, 0),
        );

        let mut extensions = InstanceExtensions::presentable();

        if !is_wayland(event_loop) {
            println!("Not using wayland!");
            extensions.wayland_surface = false;
        } else {
            println!("On Wayland");
        }

        //Currently loading debug layers always
        //TODO put behind a debug flag

        let layer = InstanceLayer::debug_layers();

        let debug = Debug::errors();
        #[cfg(Debug)]
        let debug = Debug::all(); //Overwrite for debug builds with all

        let instance =
            Instance::new(Some(app_info), Some(extensions), Some(layer), Some(debug)).unwrap();
        let surface: Arc<dyn Surface + Send + Sync> =
            WinitSurface::new(instance.clone(), window, event_loop).unwrap();
        //Now search for any graphics capable device
        let (physical_device, _present_queue_index) =
            PhysicalDevice::find_physical_device(instance.clone())
                .unwrap()
                .into_iter()
                .filter_map(|pdev| match pdev.find_present_queue_family(&surface) {
                    Ok(queue_idx) => Some((pdev, queue_idx)),
                    Err(_) => None,
                })
                .nth(0)
                .expect("There was no graphics capable vulkan device");

        println!("Selected PhysicalDevice with families:");
        for qf in physical_device.get_queue_families() {
            println!(
                "    graphics?: {}, compute?: {}, transfer?: {}",
                qf.get_queue_type().graphics,
                qf.get_queue_type().compute,
                qf.get_queue_type().transfer
            );
        }

        //Filter out a graphics queue that also supports compute.
        //TODO when implementing async compute for updates well have to use more queues
        let queues = physical_device
            .get_queue_families()
            .iter()
            .filter_map(|q| {
                match (
                    q.get_queue_type().graphics,
                    q.get_queue_type().compute,
                    q.get_queue_type().transfer,
                ) {
                    (true, true, _) => Some((*q, 1.0 as f32)),
                    _ => None,
                }
            })
            .collect::<Vec<_>>();

        assert!(
            queues.len() > 0,
            "Could not find correct graphics/compute queue"
        );

        //TODO maybe enable debug marker here if debug flag is set
        let features = *physical_device.get_features();
        let vulkan_memory_model = PhysicalDeviceVulkan12Features::builder()
            .shader_int8(true)
            .vulkan_memory_model(true);

        let (device, mut queues) = DeviceBuilder::new(instance, physical_device, queues)
            .with_extension(DeviceExtension::new("VK_KHR_swapchain".to_string(), 1))
            .with_extension(DeviceExtension::new(
                "VK_KHR_vulkan_memory_model".to_string(),
                3,
            ))
            .with_device_features(features)
            .with_additional_feature(vulkan_memory_model)
            .build()
            .expect("Could not create device an queues");

        assert!(queues.len() == 1, "Could not create queue");
        let queue = queues.remove(0);
        //Since we got a vulkan instance running now, setup a swapchain
        let mut swapchain_formats = surface
            .get_supported_formats(device.get_physical_device())
            .unwrap();
        println!("Formats:");
        let format = swapchain_formats.remove(0);
        println!("    {:?}", format);
        for f in swapchain_formats {
            //TODO search crossplatform for the best
            println!("    {:?}", f);
        }

        //Since we now have our formats. Create the swapchain
        let swapchain_extent = Extent2D::builder()
            .width(window.inner_size().width as u32)
            .height(window.inner_size().height as u32)
            .build();

        let swapchain = Swapchain::new(
            device.clone(),
            surface,
            swapchain_extent,
            Some(format),
            Some(2),
            Some(PresentModeKHR::IMMEDIATE),
            Some(ImageUsage {
                color_attachment: true,
                transfer_dst: true,
                ..Default::default()
            }),
        )
        .unwrap();

        let sc_transition_fence = swapchain.images_to_present_layout(queue.clone());
        //Wait for frame transition before continuing
        sc_transition_fence.wait(u64::MAX).unwrap();

        let queues = Queues {
            graphics_queue: queue,
        };

        let layers = LayerManager::new(
            device.clone(),
            queues.clone(),
            swapchain.image_count() as usize,
        );

        let layer_combiner = LayerCombiner::new(swapchain.get_images(), device.clone(), &queues);

        MarpBackend {
            device,
            queues,
            swapchain,
            frame_extent: swapchain_extent,
            frame_idx: 0,

            layers,
            layer_combiner,
        }
    }

    fn update_camera(&mut self, id: LayerId3d, camera: Camera) {
        self.layers.update_camera(id, camera)
    }

    fn update_camera_2d(&mut self, id: LayerId2d, camera: Camera2d) {
        self.layers.update_camera_2d(id, camera);
    }

    fn update_sdf(&mut self, id: LayerId3d, new_sdf: PrimaryStream) {
        self.layers.update_sdf(id, new_sdf);
    }

    fn update_sdf_2d(&mut self, id: LayerId2d, new_sdf: PrimaryStream2d) {
        self.layers.update_sdf_2d(id, new_sdf)
    }

    fn set_layer_info(&mut self, id: LayerId, info: LayerInfo) {
        self.layers.set_info(id, info)
    }

    fn set_layer_order(&mut self, order: &[LayerId]) {
        self.layers.set_order(order);
    }

    fn new_layer(&mut self) -> LayerId3d {
        self.layers.alloc_layer_3d()
    }

    fn new_layer_2d(&mut self) -> LayerId2d {
        self.layers.alloc_layer_2d()
    }

    fn render(&mut self, window: &Window) {
        //Check that the swapchain extent matches. The higher priority is the swapchain_extent. If this is undefined the implementation can choose one, which we do based on the
        //window
        let extent = if let Ok(caps) = self.swapchain.get_suface_capabilities() {
            match caps.current_extent {
                Extent2D {
                    width: 0xFFFFFFFF,
                    height: 0xFFFFFFFF,
                } => {
                    //Choose based on the window.
                    //Todo make robust agains hidpi scaling
                    Extent2D {
                        width: window.inner_size().width,
                        height: window.inner_size().height,
                    }
                }
                Extent2D { width, height } => Extent2D { width, height },
            }
        } else {
            //Fallback to window provided size
            Extent2D {
                width: window.inner_size().width,
                height: window.inner_size().height,
            }
        };

        if self.frame_extent != extent {
            self.resize(extent);
        }

        let index = self.frame_idx;
        self.frame_idx = (self.frame_idx + 1) % self.swapchain.image_count() as usize;
        //Acquire new slot TODO make this slot the actual slot being used through out the rendering process
        let submit_image_index = self
            .swapchain
            .acquire_next_image(
                u64::MAX,
                self.layer_combiner.get_present_finished_semaphore(index),
            )
            .unwrap();

        let combine_infos = self.layers.record(index);

        //Schedule copy
        self.layer_combiner
            .combine(self.device.clone(), &self.queues, combine_infos, index);

        //Tell swapchain that it can present this frame when we finished rendering
        if let Err(_e) = self.swapchain.queue_present(
            self.queues.graphics_queue.clone(),
            vec![self.layer_combiner.get_copy_complete_semaphore(index)],
            submit_image_index,
        ) {
            println!("Dropped frame...");
        }
    }

    fn resize(&mut self, window: &Window) {
        let wext = window.inner_size();
        let new_ext = Extent2D {
            width: wext.width,
            height: wext.height,
        };
        self.resize(new_ext);
    }
}
