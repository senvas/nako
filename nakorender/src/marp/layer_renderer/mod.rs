/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use marp::ash::vk::Extent2D;

use super::layer_combiner::CombineInfo;

///Lits a scene with lights. In this case in 3d
pub(crate) mod lit3d;
///Renders a 2d stream directly to texture. Only keeps the albedo value
pub(crate) mod unlit2d;

pub trait LayerRenderer {
    type Camera;
    type Stream;

    ///Updates the render extent being used. to render into.
    fn update_extent(&mut self, new_extent: Extent2D);
    fn update_number_of_slots(&mut self, num_slots: usize);
    fn update_camera(&mut self, camera: Self::Camera);
    fn update_sdf(&mut self, sdf: Self::Stream);
    fn record(&mut self, slot: usize) -> CombineInfo;
}
