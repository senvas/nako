/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::sync::Arc;

use super::LayerRenderer;
use crate::{
    backend::LayerInfo,
    camera::Camera2d,
    marp::{
        layer_combiner::CombineInfo,
        passes::{pass::Pass, pass_primary_2d::PrimaryPass2d},
        resources::sdf_buffer::SdfBufferManager,
        Queues,
    },
};
use marp::{
    ash::vk::{self, Extent2D, PipelineStageFlags},
    buffer::Buffer,
    command_buffer::{CommandBuffer, CommandBufferPool, CommandPool},
    device::{Device, SubmitInfo},
    image::{AbstractImage, Image},
    sync::{QueueFence, Semaphore},
};
use nako::{
    glam::Vec3,
    operations::volumetric::{BoxExact, Color, Union},
    stream::{PrimaryStream, PrimaryStream2d, SecondaryStream},
};
use shared::GpuCamera2d;

pub(crate) struct Unlit2d {
    //Currently rendered to extent
    current_extent: Extent2D,
    //Handles Sdf related buffers
    sdf_buffers: SdfBufferManager,

    //Flag unset whenever sdf or camera change and the chaneg has not yet been reredndered on this slot.
    up_to_date: Vec<bool>,

    //Builds us one frame
    frame_builder: FrameBuilder,

    //vulkan context info used for creating the frame builder
    device: Arc<Device>,
    queues: Queues,
    number_of_slots: usize,

    last_camera: Camera2d,
}

impl Unlit2d {
    pub fn new(device: Arc<Device>, queues: Queues, num_slots: usize) -> Self {
        let initial_ext = Extent2D {
            width: 1,
            height: 1,
        };

        let mut initial_buffer = SdfBufferManager::new(device.clone(), &queues);
        //Add initial sdf (standard cube)
        initial_buffer.update_code(
            PrimaryStream::new()
                .push(
                    SecondaryStream::new(Union, BoxExact { extent: Vec3::ONE })
                        .push_mod(Color(Vec3::ONE))
                        .build(),
                )
                .build(),
        );

        let frame_builder = FrameBuilder::new(
            &initial_buffer,
            device.clone(),
            &queues,
            num_slots,
            initial_ext,
        );

        Unlit2d {
            current_extent: initial_ext,
            sdf_buffers: initial_buffer,
            up_to_date: vec![false; num_slots],
            frame_builder,
            device,
            queues,
            number_of_slots: num_slots,
            last_camera: Camera2d::default(),
        }
    }

    fn reset_update_state(&mut self) {
        for s in &mut self.up_to_date {
            *s = false;
        }
    }

    fn recreate(&mut self) {
        //Recreate with new properties
        self.frame_builder = FrameBuilder::new(
            &self.sdf_buffers,
            self.device.clone(),
            &self.queues,
            self.number_of_slots,
            self.current_extent,
        );
        self.up_to_date = vec![false; self.number_of_slots];
    }
}

impl LayerRenderer for Unlit2d {
    type Camera = Camera2d;
    type Stream = PrimaryStream2d;

    fn update_camera(&mut self, camera: Self::Camera) {
        self.last_camera = camera;
        self.frame_builder.update_camera(camera);
        self.reset_update_state();
    }

    fn update_sdf(&mut self, sdf: Self::Stream) {
        self.sdf_buffers.update_code_2d(sdf);
        self.frame_builder.update_sdf(self.sdf_buffers.code.clone());
        self.reset_update_state();
    }

    fn update_extent(&mut self, new_extent: Extent2D) {
        if self.current_extent.width == new_extent.width
            && self.current_extent.height == new_extent.height
        {
            return;
        }

        self.current_extent = new_extent;
        self.recreate();
        self.update_camera(self.last_camera);
    }

    fn update_number_of_slots(&mut self, num_slots: usize) {
        if num_slots == self.number_of_slots {
            return;
        }
        self.number_of_slots = num_slots;
        self.recreate();
    }

    fn record(&mut self, slot: usize) -> CombineInfo {
        if self.up_to_date[slot] {
            let image = self.frame_builder.final_image(slot);
            let ext = image.extent();
            return CombineInfo::Ready {
                image,
                //Set by the actual layer
                info: LayerInfo {
                    extent: (ext.width as usize, ext.height as usize),
                    location: (0, 0),
                },
            };
        }

        self.up_to_date[slot] = true;
        self.frame_builder.record(&self.queues, slot)
    }
}

///Single frame info for frame builder
struct FrameInfo {
    finished_execution: Arc<Semaphore>,
    command_buffer: Arc<CommandBuffer>,
    in_flight: Option<QueueFence>,
    //If some the buffer on the descriptor needs to be updated
    update_buffer: Option<Arc<Buffer>>,
}

///Responsible for building a frame. The result will be put into the swapchain image of the given
///slot.
struct FrameBuilder {
    frame_infos: Vec<FrameInfo>,
    primary_2d: PrimaryPass2d,
}

impl FrameBuilder {
    pub fn new(
        buffers: &SdfBufferManager,
        device: Arc<Device>,
        queues: &Queues,
        num_images: usize,
        extent: Extent2D,
    ) -> Self {
        let command_pool = CommandBufferPool::new(
            device.clone(),
            queues.graphics_queue.clone(),
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
        .expect("Failed to create command pool");

        let command_buffers = command_pool
            .alloc(num_images, false)
            .expect("Failed to allocate command buffers");

        let frame_infos = command_buffers
            .into_iter()
            .map(|cb| FrameInfo {
                command_buffer: cb,
                finished_execution: Semaphore::new(device.clone()).unwrap(),
                in_flight: None,
                update_buffer: None,
            })
            .collect();

        let primary_2d = PrimaryPass2d::new(&buffers, device, extent, num_images);

        FrameBuilder {
            frame_infos,
            primary_2d,
        }
    }

    pub fn update_camera(&mut self, new_camera: Camera2d) {
        //Update primary_pass
        let new_camera: GpuCamera2d = new_camera.into();

        self.primary_2d.update_camera(new_camera.clone());
    }

    pub fn final_image(&self, slot: usize) -> Arc<Image> {
        self.primary_2d.data[slot].albedo_depth.clone()
    }

    fn record_pass(
        queues: &Queues,
        mut command_buffer: Arc<CommandBuffer>,
        slot: usize,
        pass: &mut impl Pass,
    ) -> Arc<CommandBuffer> {
        //Start Primary pass
        command_buffer = pass.pre(queues, command_buffer, slot);
        command_buffer = pass.record(command_buffer, slot);
        command_buffer = pass.post(queues, command_buffer, slot);

        command_buffer
    }

    pub fn update_sdf(&mut self, buffer: Arc<Buffer>) {
        for d in &mut self.frame_infos {
            d.update_buffer = Some(buffer.clone());
        }
    }

    pub fn record(&mut self, queues: &Queues, slot: usize) -> CombineInfo {
        //Wait for the last frame on this slot if needed
        if let Some(inflight) = self.frame_infos[slot].in_flight.take() {
            inflight.wait(u64::MAX).unwrap();
        }

        //Check if we should update the buffer
        if let Some(new_buffer) = self.frame_infos[slot].update_buffer.take() {
            self.primary_2d.update_buffer(slot, new_buffer.clone());
        }

        //Reset command buffer
        let mut command_buffer = self.frame_infos[slot].command_buffer.clone();
        command_buffer.reset().unwrap();
        command_buffer
            .begin_recording(true, false, false, None)
            .unwrap();

        //Record primary pass
        command_buffer = Self::record_pass(queues, command_buffer, slot, &mut self.primary_2d);

        //At this point the shaded image is finished
        command_buffer.end_recording().unwrap();

        //Submit new frame and let it signal our Semaphore when finished
        let new_in_flight = queues
            .graphics_queue
            .queue_submit(vec![SubmitInfo::new(
                vec![], //wait for nothing since we just waited for the former fence
                vec![command_buffer],
                vec![self.frame_infos[slot].finished_execution.clone()], //Signal execution semaphore
            )])
            .unwrap();

        self.frame_infos[slot].in_flight = Some(new_in_flight);

        let ext = self.primary_2d.data[slot].albedo_depth.extent();

        CombineInfo::Rendering {
            image: self.primary_2d.data[slot].albedo_depth.clone(),
            info: LayerInfo {
                extent: (ext.width as usize, ext.height as usize),
                location: (0, 0),
            },
            sem: self.frame_infos[slot].finished_execution.clone(),
            stage: PipelineStageFlags::COMPUTE_SHADER,
        }
    }
}
