/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::path::Path;

pub const SHADER_PATH: &'static str = "nakorender/resources/shader/";

///Loads the spirv module in resource/shaders with the give name (without extension).
pub fn load_spv<'a>(name: &str) -> Option<String> {
    //test if there is this shader
    let mut path = SHADER_PATH.to_string();
    path.push_str(name);
    path.push_str(".spv");

    if !Path::new(&path).exists() {
        println!("Shader {} does not exist!", &path);
        None
    } else {
        Some(path)
    }
}
