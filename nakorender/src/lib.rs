/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
#![deny(warnings)]

pub extern crate nako;
pub use nako::glam;

pub mod backend;
///Marp based backend
pub mod marp;

#[cfg(feature = "cpu_backend")]
pub mod cpu;

///Different camera related tools and imlpementations
pub mod camera;

pub use marp_surface_winit::winit;
