/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use nako::glam::{self, Mat4, Vec2, Vec3};
use shared::{GpuCamera, GpuCamera2d};

///definition for a camera in 3d space
#[derive(Clone, Copy)]
pub struct Camera {
    pub location: glam::Vec3,
    pub rotation: glam::Quat,
    ///Horizontal field of view in degree.
    pub fov: f32,
}

impl Camera {
    pub fn transform(&self) -> Mat4 {
        glam::Mat4::from_rotation_translation(self.rotation, self.location)
    }
}

impl Default for Camera {
    fn default() -> Self {
        Camera {
            location: Vec3::ZERO,
            rotation: glam::Quat::IDENTITY,
            fov: 90.0,
        }
    }
}

impl From<Camera> for GpuCamera {
    fn from(cam: Camera) -> Self {
        let transform = glam::Mat4::from_rotation_translation(cam.rotation, cam.location);

        GpuCamera {
            location: [cam.location.x, cam.location.y, cam.location.z],
            fov: cam.fov, //TODO already calculate radiant?
            transform: transform.to_cols_array_2d(),
            img_width: 800,
            img_height: 600,
            pad1: [0, 0],
        }
    }
}

///Definition of an camera in 2d space
#[derive(Clone, Copy)]
pub struct Camera2d {
    ///location of the Top left edge of this camera.
    pub location: Vec2,
    ///Extent of this camera in "worldspace" units. Is projected to the actual layer,
    pub extent: Vec2,
    ///Rotations around it self in degree.
    pub rotation: f32,
}

impl Camera2d {
    ///Calculates where in 2d worldspace a pixel would be
    pub fn pixel_to_worldspace(
        &self,
        x: usize,
        y: usize,
        img_width: usize,
        img_height: usize,
    ) -> Vec2 {
        //TODO impl rotation as well
        let pixel_size = self.extent / Vec2::new(img_width as f32, img_height as f32);

        self.location + (pixel_size * Vec2::new(x as f32, y as f32))
    }
}

impl Default for Camera2d {
    fn default() -> Self {
        Camera2d {
            location: Vec2::ZERO,
            extent: Vec2::new(1.0, 1.0),
            rotation: 0.0,
        }
    }
}

impl From<Camera2d> for GpuCamera2d {
    fn from(camera: Camera2d) -> Self {
        GpuCamera2d {
            location: [camera.location.x, camera.location.y],
            extent: [camera.extent.x, camera.extent.y],
            rotation: camera.rotation,
            pad1: [0.0; 3],
        }
    }
}
