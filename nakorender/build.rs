/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use std::{
    io::{Read, Write},
    path::PathBuf,
};

const SHADER_PATHS: &'static str = "resources/shader";

fn glsl_include(
    file_name: &str,
    _include_type: shaderc::IncludeType,
    _target_shader: &str,
    _size: usize,
) -> core::result::Result<shaderc::ResolvedInclude, String> {
    //Load the file given in the include and load it
    let mut content = String::new();
    let mut include_file = std::fs::File::open(&to_include_path(file_name))
        .expect("Failed to open shader include file!");
    include_file
        .read_to_string(&mut content)
        .expect("Could not read include file to shader!");

    Ok(shaderc::ResolvedInclude {
        resolved_name: String::from(file_name),
        content,
    })
}

fn to_include_path(name: &str) -> String {
    SHADER_PATHS.to_string() + "/glsl/includes/" + name
}

fn to_spath(name: &str) -> String {
    SHADER_PATHS.to_string() + "/glsl/" + name
}

fn compile_shader(name: &str, out_name: &str, shader_type: shaderc::ShaderKind) -> PathBuf {
    let file_path = to_spath(name);

    let mut file = std::fs::File::open(&file_path).expect("Failed to read shader!");
    let mut source = String::new();
    file.read_to_string(&mut source)
        .expect("Failed to read shader!");

    let mut compiler = shaderc::Compiler::new().expect("Failed to start shaderc compiler!");

    let mut compile_options =
        shaderc::CompileOptions::new().expect("Failed to create compile options!");
    compile_options.add_macro_definition("EP", Some("main"));
    //Always go for performance
    compile_options.set_optimization_level(shaderc::OptimizationLevel::Performance);
    compile_options.set_include_callback(glsl_include);

    println!("COMPILING!");
    let result = compiler
        .compile_into_spirv(
            &source,
            shader_type,
            name,
            "main", //always entry at main atm.
            Some(&compile_options),
        )
        .expect("Failed to compile shader!");
    println!("FINISHED");
    if result.get_num_warnings() > 0 {
        println!("\tWarnings: {}", result.get_warning_messages());
    }

    let out_path = to_spath(out_name);
    let mut out_file = std::fs::File::create(&out_path).expect("Could not create spv file!");
    let len = out_file
        .write(result.as_binary_u8())
        .expect("Failed to write out binary spv file");
    if len != result.as_binary_u8().len() {
        panic!("Failed to write out shader binary!");
    }

    PathBuf::from(out_path)
}
fn exchange_ending(src: &str, new_ending: &str) -> String {
    let (name, _old_ending) = src.rsplit_once('.').unwrap();

    let mut name = name.to_string();
    name.push_str(new_ending);
    name
}
fn compiler(name: &str) {
    let shader_path = match (
        name.ends_with(".comp"),
        name.ends_with(".glsl"),
        name.ends_with(".vs"),
        name.ends_with(".fs"),
    ) {
        (true, _, _, _) | (_, true, _, _) => {
            println!("cargo:warning=Compiling {} as  compute shader", name);
            let spirv_name = exchange_ending(&name, ".spv");
            compile_shader(&&name, &spirv_name, shaderc::ShaderKind::Compute)
        }
        (_, _, true, _) => {
            println!("cargo:warning=Compiling {} as vertex shader", name);
            let spirv_name = exchange_ending(&name, ".spv");
            compile_shader(&&name, &spirv_name, shaderc::ShaderKind::Vertex)
        }
        (_, _, _, true) => {
            println!("cargo:warning=Compiling {} as fragment shader", name);
            let spirv_name = exchange_ending(&name, ".spv");
            compile_shader(&&name, &spirv_name, shaderc::ShaderKind::Fragment)
        }
        _ => {
            println!("cargo:warning=Could not classify shader {}", name);
            return;
        }
    };

    println!("cargo:warning=Compiled shader to {:?}", shader_path);
}

fn main() {
    // Tell Cargo that if the given file changes, to rerun this build script.
    //println!("cargo:rerun-if-changed=../nakorender/src/lib.rs");

    for file in std::fs::read_dir(&(SHADER_PATHS.to_string() + "/glsl")).unwrap() {
        let name = file
            .unwrap()
            .file_name()
            .into_string()
            .unwrap()
            .rsplit('/')
            .last()
            .unwrap()
            .to_string();
        if !name.ends_with(".spv") {
            compiler(&name);
        }
    }
}
