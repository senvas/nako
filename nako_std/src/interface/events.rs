/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use nako::glam::Vec2;

use crate::Area;

///Collection of meta events. Some ui elements implement `EventConsumer`. However, you are always free to call
/// the elements state changing functions yourself
#[derive(Clone, Copy, PartialEq)]
pub enum MetaEvent {
    None,
    ///If a selection happended and where on the screen
    Select(Vec2),
    Drag {
        drag_start: Vec2,
        current_location: Vec2,
    },
    DragEnd,
    CursorMoved(Vec2),
    Resize(Area),
}
