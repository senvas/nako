/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
//! # Interface
//! The crate provides several interface elements that might be of use, like buttons, editing fields or the ability to draw
//! Arbitrary lines of Bezier splines.

use self::events::MetaEvent;
pub mod buttons;
///Provides abstract events that can be used to modify interface element state.
pub mod events;
pub mod splines;

pub trait EventConsumer {
    fn update(&mut self, event: MetaEvent);
}
