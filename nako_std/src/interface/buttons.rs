/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use nako::{
    glam::{Vec2, Vec3},
    operations::volumetric::{Color, Round, Union},
    stream::{Primary2dBuilder, PrimaryStream2d, SecondaryStream2d},
};

use crate::{text::TextLine, Area};

use super::{events::MetaEvent, EventConsumer};

pub const DEFAULT_BUTTON_SIZE: (f32, f32) = (100.0, 50.0);

///Simple Button that executes an event when pushed.
///It also allows you to execute code when hovering and the like.
pub struct PushButton<C> {
    ///Area this button covers on screen
    area: Area,

    pub border_radius: f32,
    pub color: Color,
    pub border_color: Color,
    pub on_hover_color: Option<Color>,

    pub context: C,
    ///Function that should be executed when pressing on the button. Gets the context variable of this button as well as the
    ///Click location.
    pub press_event: Box<dyn Fn(&mut C, Vec2)>,

    cursor_over_button: bool,
    state_changed: bool,
}

impl<C> PushButton<C> {
    pub fn with_ctx<NC>(
        self,
        new_function: impl Fn(&mut NC, Vec2) + 'static,
        new_context: NC,
    ) -> PushButton<NC> {
        PushButton {
            context: new_context,
            press_event: Box::new(new_function),

            area: self.area,
            border_color: self.border_color,
            border_radius: self.border_radius,
            color: self.color,
            cursor_over_button: self.cursor_over_button,
            on_hover_color: self.on_hover_color,
            state_changed: true,
        }
    }

    pub fn with_area(mut self, area: Area) -> Self {
        self.set_area(area);
        self
    }

    pub fn set_area(&mut self, area: Area) {
        self.area = area;
        self.cursor_over_button = false; //reset state, cannot know
    }

    pub fn on_press(&mut self, location: Vec2) {
        if self.cursor_over_button {
            (*self.press_event)(&mut self.context, location);
            self.state_changed = true;
        }
    }

    ///Must be called if the cursor moved, might change inner state of the button
    pub fn on_cursor_move(&mut self, new_location: Vec2) {
        let old_state = self.cursor_over_button;
        self.cursor_over_button = self.area.is_in(new_location);
        if old_state != self.cursor_over_button {
            self.state_changed = true;
        }
    }

    pub fn record_button(&mut self, mut stream: Primary2dBuilder) -> Primary2dBuilder {
        //Append this button, assuming area is on "world" space
        let (mut outer_box, center_offset) = self.area.as_box_2d();

        //remove radius from box size so combensate for rounding
        outer_box.extent -= Vec2::new(self.border_radius, self.border_radius);

        stream = stream.push(
            SecondaryStream2d::new(Union, outer_box)
                .push_mod(center_offset)
                .push_mod(Round {
                    radius: self.border_radius,
                })
                .push_mod(if self.cursor_over_button {
                    self.on_hover_color.unwrap_or(self.color)
                } else {
                    self.color
                })
                .build(),
        );
        self.state_changed = false;
        stream
    }

    pub fn state_changed(&self) -> bool {
        self.state_changed
    }
}

impl Default for PushButton<()> {
    fn default() -> Self {
        PushButton {
            area: Area::ONE,
            border_color: Color(Vec3::ONE),
            border_radius: 10.0,
            color: Color(Vec3::new(0.5, 0.5, 0.5)),
            on_hover_color: None,
            cursor_over_button: false,

            context: (),
            press_event: Box::new(|_c, _v| {}),

            state_changed: true,
        }
    }
}

impl<C> EventConsumer for PushButton<C> {
    fn update(&mut self, event: MetaEvent) {
        match event {
            MetaEvent::Select(loc) => self.on_press(loc),
            MetaEvent::CursorMoved(to) => self.on_cursor_move(to),
            MetaEvent::Resize(_) => self.state_changed = true,
            _ => {}
        }
    }
}

pub struct LabeledButton<B> {
    pub button: B,
    pub label: TextLine,
}

impl<B: Default> Default for LabeledButton<B> {
    fn default() -> Self {
        LabeledButton {
            button: B::default(),
            label: TextLine::default(),
        }
    }
}

impl<C: EventConsumer> EventConsumer for LabeledButton<C> {
    fn update(&mut self, event: MetaEvent) {
        self.button.update(event)
    }
}

impl<C> LabeledButton<PushButton<C>> {
    pub fn set_area(&mut self, new_area: Area) {
        self.button.set_area(new_area);
        self.label.fit_into_area(Area {
            from: new_area.from + Vec2::new(5.0, 5.0),
            to: new_area.to - Vec2::new(5.0, 5.0),
        });
        //get resulting area and position in the middl
        let new_text_area = self.label.get_area();
        let offset = self.button.area.center() - new_text_area.center();
        self.label.set_position(self.label.get_position() + offset);
    }

    pub fn state_changed(&self) -> bool {
        self.button.state_changed || self.label.state_changed()
    }
    pub fn record_button(&mut self, stream: Primary2dBuilder) -> Primary2dBuilder {
        self.button.record_button(stream)
    }
    pub fn record_label(&mut self, stream: Primary2dBuilder) -> Primary2dBuilder {
        self.label.record_line(stream)
    }
}
