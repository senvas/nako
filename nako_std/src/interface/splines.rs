/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use nako::{
    glam::{Vec2, Vec3},
    operations::{
        planar::primitives2d::Bezier2d,
        volumetric::{Color, Union},
    },
    stream::{Primary2dBuilder, SecondaryStream2d},
};

pub struct Segment {
    pub start: Vec2,
    pub ctrl: Vec2,
    pub end: Vec2,
}

///Allows you to define a bezier spline made from several spline segments.
///There are several methodes for defining a spline.
pub struct Spline {
    pub width: f32,
    pub segments: Vec<Segment>,
}

impl Spline {
    pub fn from_segements(segments: Vec<Segment>) -> Self {
        Spline {
            width: 1.0,
            segments,
        }
    }

    pub fn with_width(mut self, width: f32) -> Self {
        self.width = width;
        self
    }

    fn ctrl_point(start: Vec2, target: Vec2, after_target: Vec2) -> Vec2 {
        let dir = (after_target - target)
            .normalize()
            .lerp((target - start).normalize(), 0.5);

        //From target, search backwarts on dir for either a matching y or match x coord
        let mut ctrl = match (dir.x > 0.0, dir.y > 0.0) {
            (false, false) | (true, true) => {
                //Solve for y coord and derive x
                let dir = -1.0 * dir;

                let x = -1.0 * ((start.y - target.y) / dir.y);

                let point = target - x * dir;
                point
            }
            (true, false) | (false, true) => {
                //solve for x and derive y
                let dir = -1.0 * dir;
                let y = -1.0 * ((start.x - target.x) / dir.x);

                let point = target - y * dir;
                point
            }
        };

        if ctrl.x == f32::INFINITY {
            ctrl.x = f32::MAX;
        }

        if ctrl.x == f32::NEG_INFINITY {
            ctrl.x = f32::MIN;
        }

        if ctrl.x.is_nan() {
            ctrl.x = 0.0;
        }

        if ctrl.y == f32::INFINITY {
            ctrl.y = f32::MAX;
        }

        if ctrl.y == f32::NEG_INFINITY {
            ctrl.y = f32::MIN;
        }

        if ctrl.y.is_nan() {
            ctrl.y = 0.0;
        }

        println!("Found {} for s={}, end={}", ctrl, start, target);
        ctrl
    }

    ///Computes the splines control points in a way that the spline always
    ///Goes "through" the `points`
    ///
    /// # Panics
    /// `points` must be at least 2 long (start/end), otherwise the function will
    /// panic.
    pub fn from_points(points: Vec<Vec2>) -> Self {
        debug_assert!(points.len() >= 2, "Number of points was less then 2");

        let mut segments = Vec::with_capacity(points.len() - 1);

        for i in 0..(points.len() - 2) {
            segments.push(Segment {
                start: points[i],
                end: points[i + 1],
                ctrl: Self::ctrl_point(points[i], points[i + 1], points[i + 2]),
            })
        }

        Spline {
            width: 1.0,
            segments,
        }
    }

    fn record_spline(&self, mut stream: Primary2dBuilder) -> Primary2dBuilder {
        for seg in &self.segments {
            stream = stream.push(
                SecondaryStream2d::new(
                    Union,
                    Bezier2d {
                        start: seg.start,
                        controll: seg.ctrl,
                        end: seg.end,
                        width: self.width,
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .build(),
            );
        }

        stream
    }

    fn state_changed(&self) -> bool {
        //Cannot happen at the moment
        false
    }
}
