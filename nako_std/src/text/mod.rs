/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
//! # Text
//! Provides everything you need related to text. We use font_kit to retriev fonts and rustybuzz to translate fonts and
//! text into usable signed distance fields functions.

use std::{fs::File, io::Read, mem::needs_drop, path::Path};

use font_kit::{
    error::FontLoadingError,
    font::Font,
    hinting::HintingOptions,
    outline::{Outline, OutlineBuilder},
};

use nako::{
    glam::{Vec2, Vec3},
    operations::{
        planar::{
            modifiers2d::Translate2d,
            primitives2d::{Circle, Polygone2d},
        },
        volumetric::{Color, Subtraction, Union},
    },
    stream::{Primary2dBuilder, SecondaryStream2d},
};
use rustybuzz::{shape, Face, UnicodeBuffer};

use crate::{text::helper::scanline_contoure, Area};

use self::helper::tesselate_contour;

//Contains helper common to all character realated tasks.
mod helper;

///Native character shape information
pub struct CharacterShape {
    ///If conture should be subtracted or unioned to the already existing shape
    is_subtract: bool,
    shape: Polygone2d,
    area: Area,
}

//TODO
// Therfore we should use rustybuzz for shaping the glyphs, the use fontkit for get the correct outlines,
// give those to lyon for tesselation and then build the vector of tesselated triangles, or better a single
// big "outline" polygone from which the holes are cut via Subtraction within the sdf.

///A single character nased on some font. The character is guaranteed to have at max a size of `size` * `size`.
pub struct Character {
    ///The character represented
    character: char,
    ///Characters outlines, used for retesselating when changing size.
    outline: Outline,
    ///Size of this character on its layer. A character is guaranteed to not exceed a boundingbox of `Box2d{from: (0.0, 0.0), to: (size, size)}`
    pub size: f32,
    ///Position at which the character is drawn.
    pub position: Vec2,
    ///Shape parts that make up this character.
    pub shapes: Vec<CharacterShape>,
    pub color: Vec3,
    ///Factor used when re tesselating a character on size change. Increase if you are getting performance problems with a lot of text.
    tess_factor: f32,

    state_changed: bool,
}

impl Character {
    pub const DEFAULT_TESSELATION_FACTOR: f32 = 1.0;

    pub fn from_file(file: impl AsRef<Path>, character: char) -> Result<Self, FontLoadingError> {
        let font = Font::from_path(file, 0)?;
        Ok(Self::from_font(&font, character))
    }

    ///Loads the character from the font. Have a look at `font-kit` on how to load such a font.
    ///There are ways to dynamically determin a correct font on the system based on parameters, of by specifying a file.
    ///
    /// # Panics
    /// Note that the function will panic if the font contains quadric bezier splines (for now). Only Cubic bezier splines are supported at the moment.
    pub fn from_font(font: &Font, character: char) -> Self {
        let id = if let Some(glyph_id) = font.glyph_for_char(character) {
            glyph_id
        } else {
            panic!("No glyph for char in font");
        };

        Self::from_glyph_id(font, id, character)
    }

    pub fn from_glyph_id(font: &Font, id: u32, character: char) -> Self {
        let add = font.metrics().descent.abs();
        let div = add + font.metrics().ascent;

        let mut outline_builder = OutlineBuilder::new();
        font.outline(id, HintingOptions::None, &mut outline_builder)
            .unwrap();

        let mut outline = outline_builder.into_outline();

        //Prepare outline infos for
        for c in &mut outline.contours {
            for p in &mut c.positions {
                p.set_x(p.x() / div);
                p.set_y(1.0 - (p.y() + add) / div); //flip y
            }
        }

        //Now tesselate each counture. Based on the winding order, either Union or subtract the polygone from the
        // object
        let shapes = outline
            .contours
            .iter()
            .map(|c| tesselate_contour(c, 0.01))
            .collect::<Vec<_>>();

        let shapes = scanline_contoure(shapes);

        Character {
            character,
            outline,
            size: 1.0,
            shapes,
            position: Vec2::ZERO,
            color: Vec3::ONE,
            tess_factor: Self::DEFAULT_TESSELATION_FACTOR,

            state_changed: true,
        }
    }

    ///re tesselates self based on current size
    fn retesselate(&mut self) {
        let tesselation_factor = self.tess_factor / self.size;
        let shapes = self
            .outline
            .contours
            .iter()
            .map(|c| tesselate_contour(c, tesselation_factor))
            .collect::<Vec<_>>();

        let shapes = scanline_contoure(shapes);
        //Mark as changed
        self.state_changed = true;
        self.shapes = shapes;
    }

    pub fn with_tesselation_factor(mut self, factor: f32) -> Self {
        self.tess_factor = factor;
        self.retesselate();
        self
    }

    pub fn with_size(mut self, size: f32) -> Self {
        self.size = size;
        self.retesselate();
        self
    }

    pub fn set_size(&mut self, size: f32) {
        self.size = size;
        self.retesselate();
    }

    pub fn with_position(mut self, position: Vec2) -> Self {
        self.position = position;
        self
    }

    pub fn get_char(&self) -> &char {
        &self.character
    }

    pub fn get_bbox(&self) -> Area {
        Area {
            from: self
                .shapes
                .iter()
                .fold(Vec2::new(f32::INFINITY, f32::INFINITY), |com, s| {
                    com.min(s.area.from * self.size + self.position)
                }),
            to: self
                .shapes
                .iter()
                .fold(Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY), |com, s| {
                    com.max(s.area.to * self.size + self.position)
                }),
        }
    }

    pub fn record_character(&mut self, mut stream: Primary2dBuilder) -> Primary2dBuilder {
        for s in &self.shapes {
            let mut poly = s.shape.clone();
            for coord in poly.vertices.iter_mut() {
                *coord *= self.size;
            }

            /*NOTE turn on to show poly's control points
            for (i, c) in poly.vertices.iter().enumerate(){
                stream = stream.push(
                    SecondaryStream2d::new(
                        Union,
                        Circle{radius: 2.0}
                    ).push_mod(Translate2d(*c + self.position))
                        .push_mod(Color(
                            Vec3::new(1.0, i as f32 / poly.vertices.len() as f32, 0.0)
                        )).build()
                );
            }
            */

            let sub = if s.is_subtract {
                SecondaryStream2d::new(Subtraction, poly)
            } else {
                SecondaryStream2d::new(Union, poly)
            };

            stream = stream.push(
                sub.push_mod(Translate2d(self.position))
                    .push_mod(Color(self.color))
                    .build(),
            );
        }

        /*
        for s in &self.shapes{
            let mid = s.shape.vertices.iter().fold(Vec2::ZERO, |com, v| com + *v) / s.shape.vertices.len() as f32;

            stream = stream.push(
                SecondaryStream2d::new(
                    Union,
                    Circle{radius: 10.0}
                )
                    .push_mod(Translate2d(self.position + mid * self.size))
                    .push_mod(Color(if s.is_subtract{Vec3::new(1.0, 1.0, 0.0)}else{Vec3::new(0.0, 1.0, 1.0)}))
                    .build()
            );
        }
         */
        //Since we appended now
        self.state_changed = false;
        stream
    }
    pub fn state_changed(&self) -> bool {
        self.state_changed
    }
}

pub struct LineCharacter {
    pub character: Character,
    ///Base offset when scaling is 1.0. Used to calculate correct offset when scaling the line
    position: Vec2,
}

///A line of text based on a string laied out by rustybuzz. Note that linebreaks are *NOT* supported. Those will lead to problems when laying out the characters.
pub struct TextLine {
    content: String,
    characters: Vec<LineCharacter>,
    position: Vec2,
    pub size: f32,
    pub color: Vec3,
    state_changed: bool,
}

impl TextLine {
    pub fn new_from_path(
        font_path: impl AsRef<Path>,
        string: &str,
    ) -> Result<Self, FontLoadingError> {
        let file = File::open(font_path)?;
        Ok(Self::new(file, string))
    }

    ///Shapes a new text line from the given font, the string and the size in "points per em".
    pub fn new(mut font_file: File, string: &str) -> Self {
        //Could be used to select a sub font. However, currently not implemented.
        let font_index = 0;
        //Load font in both font-kit and rustybuzz by first loading its bytes and the passing them to both
        let mut font_data = Vec::new();
        font_file.read_to_end(&mut font_data).unwrap();

        let fontkit = Font::from_file(&mut font_file, font_index).unwrap();
        let font_face = Face::from_slice(&font_data, font_index).unwrap();

        let add = fontkit.metrics().descent.abs();
        let size_div = add + fontkit.metrics().ascent;

        let mut buffer = UnicodeBuffer::new();
        buffer.push_str(string);

        //TODO at this point we could set stuff like direction etc. However that is currently not configureable.

        let glyphs = shape(&font_face, &[], buffer);

        let (_, characters) = glyphs
            .glyph_positions()
            .iter()
            .zip(glyphs.glyph_infos().iter())
            .fold((0.0, Vec::new()), |(offset, mut glyphs), (pos, info)| {
                //println!("g = {:?}, pos = {:?}", offset, info);

                glyphs.push(LineCharacter {
                    character: Character::from_glyph_id(&fontkit, info.glyph_id, '-')
                        .with_position(Vec2::new(offset, 0.0)),
                    position: Vec2::new(offset, 0.0),
                });

                (offset + (pos.x_advance as f32 / size_div), glyphs)
            });

        TextLine {
            content: string.to_string(),
            characters,
            position: Vec2::ZERO,
            size: 1.0,
            color: Vec3::ONE,
            state_changed: true,
        }
    }

    pub fn update_line(&mut self) {
        //scale each character
        for c in self.characters.iter_mut() {
            c.character.size = self.size;
            c.character.position = self.position + (c.position * self.size);
        }
        self.state_changed = true;
    }

    pub fn with_size(mut self, size: f32) -> Self {
        self.size = size;
        self.update_line();
        self
    }

    pub fn with_position(mut self, position: Vec2) -> Self {
        self.position = position;
        self.update_line();
        self
    }

    pub fn set_position(&mut self, pos: Vec2) {
        self.position = pos;
        self.update_line();
    }

    pub fn get_position(&self) -> Vec2 {
        self.position
    }

    pub fn with_color(mut self, color: Vec3) -> Self {
        self.color = color;
        for g in &mut self.characters {
            g.character.color = color;
        }
        self
    }

    ///Resizes the line in a way that it fits into "area"
    pub fn fit_into_area(&mut self, area: Area) {
        println!("Fitting to: {:?}", area);
        //Calculate new size, based on current size and the therefore resulting area
        let current_area = self.get_area();
        let scaling = area.extent() / current_area.extent();

        let scaled_extent = current_area.extent() * scaling;
        println!("Scaled: {}", scaled_extent);
        //assert!(scaled_extent.x <= area.extent().x && scaled_extent.y <= area.extent().y, "scaling doesnt fit");

        let scaling = scaling.x;
        /*
                let scaling = if (current_area.extent().x * scaling.x) <= area.extent().x && (current_area.extent().y * scaling.x) <= area.extent().y{
                    scaling.x
                }else{
                    println!("Falling back");
                    scaling.y
                };
        */

        //Update inner size
        self.size = self.size * scaling;
        self.position = area.from;
        //Update actual line
        self.update_line();
    }

    pub fn get_str(&self) -> &str {
        &self.content
    }

    pub fn get_area(&self) -> Area {
        self.characters.iter().fold(
            Area {
                from: Vec2::new(f32::INFINITY, f32::INFINITY),
                to: Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY),
            },
            |mut com, ch| {
                com.from = com.from.min(ch.character.get_bbox().from);
                com.to = com.to.max(ch.character.get_bbox().to);

                com
            },
        )
    }

    pub fn record_line(&mut self, mut stream: Primary2dBuilder) -> Primary2dBuilder {
        for c in &mut self.characters {
            stream = c.character.record_character(stream);
        }
        self.state_changed = false;
        stream
    }

    pub fn state_changed(&self) -> bool {
        self.state_changed
    }
}

impl Default for TextLine {
    fn default() -> Self {
        TextLine {
            characters: vec![],
            color: Vec3::ONE,
            content: "".to_string(),
            position: Vec2::ZERO,
            size: 1.0,
            state_changed: true,
        }
    }
}
