/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

use crate::{
    operations::volumetric::custom::SkipOperator,
    sdf::BBox,
    serialize::{StreamSerializer, Word},
};

///Must be implemented by any modifier, that assumes an already present Primitve in the stream.
pub trait Modifier {
    ///Used to enqueue instructions before calling the childs instructions
    fn pre(&self, _ser: &mut StreamSerializer) {}
    ///Used to enqueue instuctions after calling the childs instructions
    fn post(&self, _ser: &mut StreamSerializer) {}
}

pub trait Primitive {
    fn bound(&self) -> BBox;
    fn serialize(&self, ser: &mut StreamSerializer);
}

pub trait Combinator {
    fn serialize(&self, ser: &mut StreamSerializer);
    ///Is called after assembling the secondary stream. Might be used to adjust the bound based on the
    ///combinator
    fn bound_modifier(&self, _secondary_bound: &mut BBox) {}
}

#[derive(Clone)]
pub struct TipInfo {
    pub bbox: BBox,
}

///Secondary stream. Is build from a linear stream of secondary operations. Mostly primitives and attributes, like translation or color.
#[derive(Clone)]
pub struct SecondaryStream {
    tip_info: TipInfo,
    substream: Vec<Word>,
}

impl SecondaryStream {
    pub fn new(
        combinator: impl Combinator + 'static,
        primitive: impl Primitive + 'static,
    ) -> SecondaryStreamBuilder {
        SecondaryStreamBuilder {
            primitive: Box::new(primitive),
            combinator: Box::new(combinator),
            mod_stream: Vec::new(),
        }
    }
}

pub struct SecondaryStreamBuilder {
    ///Base primitive for this secondary stream
    primitive: Box<dyn Primitive>,
    ///ombinator used for thi secondary stream to be added to the primary stream,
    combinator: Box<dyn Combinator>,
    ///All modifiers in order, where [0] is the first after the primitve and [len] is the last modifier applied, before
    ///merging with the primary stream.
    mod_stream: Vec<Box<dyn Modifier>>,
}

impl SecondaryStreamBuilder {
    pub fn push_mod(mut self, attr: impl Modifier + 'static) -> Self {
        self.mod_stream.push(Box::new(attr));
        self
    }

    pub fn build(self) -> SecondaryStream {
        let mut ser = StreamSerializer::new();

        for m in self.mod_stream.iter().rev() {
            m.pre(&mut ser);
        }

        //setup initial bounding box
        ser.on_bbox(|bbox| *bbox = self.primitive.bound());

        self.primitive.serialize(&mut ser);
        for m in self.mod_stream.iter() {
            m.post(&mut ser);
        }

        //let the combinator modify the tip info
        self.combinator.bound_modifier(&mut ser.bbox);
        //Now serialize the combinator as well
        self.combinator.serialize(&mut ser);

        let (tip_info, substream) = ser.consume();

        SecondaryStream {
            tip_info,
            substream,
        }
    }
}

#[derive(Clone)]
pub struct PrimaryStreamBuilder {
    primary_stream: Vec<SecondaryStream>,
}

impl PrimaryStreamBuilder {
    pub fn push(mut self, secondary_stream: SecondaryStream) -> Self {
        self.primary_stream.push(secondary_stream);
        self
    }

    //TODO document what happens here. In essence we build a BVH for the primary stream. Since we know for each secondary stream in which worldspace
    //region the global SDF is changed, we can savely isolate Clusters in which some operations in the primary stream are ignored.
    //We then inline all clusters in a new big Bvh where the leafes represent the substream needed to be evaluate.
    pub fn build(self) -> PrimaryStream {
        //TODO this algorithm works, but is far from fast. Should refactor at some point
        //let mut cluster: Vec<_> = self.primary_stream.into_iter().enumerate().map(|(idx, v)| (idx, v)).collect();

        let mut clusters = Cluster::from_stream(self.primary_stream.clone());
        //println!("Found {} disjunct clusters", clusters.len());

        insert_skip_ops(&mut clusters);

        let global_stream = clusters.into_iter().fold(vec![], |mut stream, c| {
            stream.append(&mut c.into_code_stream());
            stream
        });

        PrimaryStream {
            stream: global_stream,
        }
    }
}

#[derive(Clone)]
pub struct PrimaryStream {
    ///Gpu uploadable buffer. Contains a mixture of BVH nodes and instuction streams.
    pub stream: Vec<Word>,
}

impl PrimaryStream {
    pub fn new() -> PrimaryStreamBuilder {
        PrimaryStreamBuilder {
            primary_stream: Vec::new(),
        }
    }

    pub fn empty() -> Self {
        PrimaryStream { stream: vec![] }
    }
}

pub trait Primitive2d {
    fn ser(&self, _ser: &mut StreamSerializer);
    fn bound(&self) -> BBox;
}

pub trait Combinator2d {
    fn ser(&self, _ser: &mut StreamSerializer);
    fn bound_modifier(&self, _bound: &mut BBox) {}
}

pub trait Modifier2d {
    fn pre(&self, _ser: &mut StreamSerializer) {}
    fn post(&self, _ser: &mut StreamSerializer) {}
}

///Same as the normal secondary stream, but only for 2d primitives and mods
#[derive(Clone)]
pub struct SecondaryStream2d {
    pub stream: Vec<Word>,
    pub bound: BBox,
}

impl SecondaryStream2d {
    pub fn new(
        combinator: impl Combinator2d + 'static,
        primitive: impl Primitive2d + 'static,
    ) -> Secondary2dBuilder {
        Secondary2dBuilder {
            primitive: Box::new(primitive),
            combinator: Box::new(combinator),
            modifier: vec![],
        }
    }

    pub fn code_len(&self) -> usize {
        self.stream.len()
    }
}

pub struct Secondary2dBuilder {
    primitive: Box<dyn Primitive2d>,
    combinator: Box<dyn Combinator2d>,
    modifier: Vec<Box<dyn Modifier2d>>,
}

impl Secondary2dBuilder {
    pub fn push_mod(mut self, modifier: impl Modifier2d + 'static) -> Self {
        self.modifier.push(Box::new(modifier));
        self
    }

    pub fn build(self) -> SecondaryStream2d {
        let mut ser = StreamSerializer::new();
        for m in &self.modifier {
            m.pre(&mut ser);
        }
        self.primitive.ser(&mut ser);
        for m in &self.modifier {
            m.post(&mut ser);
        }
        self.combinator.bound_modifier(&mut ser.bbox);
        self.combinator.ser(&mut ser);

        let (tipinfo, stream) = ser.consume();

        SecondaryStream2d {
            stream,
            bound: tipinfo.bbox,
        }
    }
}

///Primary stream for planar sdfs
#[derive(Clone)]
pub struct PrimaryStream2d {
    //The whole 2d stream
    pub stream: Vec<Word>,
    pub bound: BBox,
}

impl PrimaryStream2d {
    pub fn new() -> Primary2dBuilder {
        Primary2dBuilder { subs: Vec::new() }
    }
}

pub struct Primary2dBuilder {
    subs: Vec<SecondaryStream2d>,
}

impl Primary2dBuilder {
    pub fn push(mut self, s: SecondaryStream2d) -> Self {
        self.subs.push(s);
        self
    }

    pub fn build(self) -> PrimaryStream2d {
        let mut stream = Vec::with_capacity(self.subs.iter().fold(0, |l, c| l + c.code_len()));

        let mut bound = if let Some(first_stream) = self.subs.first() {
            first_stream.bound
        } else {
            BBox::default()
        };

        for mut s in self.subs {
            bound = bound.union(s.bound);
            stream.append(&mut s.stream);
        }

        PrimaryStream2d { stream, bound }
    }
}

struct Cluster {
    bound: BBox,
    combinators: Vec<(usize, SecondaryStream)>,
    skip_operator: Option<SkipOperator>,
}

impl Cluster {
    //Number of words this cluster occupies
    fn code_len(&self) -> usize {
        self.combinators
            .iter()
            .fold(0, |size, (_idx, c)| size + c.substream.len())
    }

    fn insert_skip(&mut self) {
        self.skip_operator = Some(SkipOperator {
            skip_to: self.code_len() as u32,
            bound: self.bound,
        });
    }

    fn into_code_stream(self) -> Vec<Word> {
        let mut stream =
            Vec::with_capacity(self.code_len() + if self.skip_operator.is_some() { 5 } else { 0 });
        /*
                println!(
                    "Serialzing cluster with {} primarys and skip op: {}",
                    self.combinators.len(),
                    self.skip_operator.is_some()
                );
        */
        if let Some(skipop) = self.skip_operator {
            let mut ser = StreamSerializer::new();
            skipop.serialize(&mut ser);
            let (_tip, mut skipop_stream) = ser.consume();
            //TODO do we need the skip op bound?
            stream.append(&mut skipop_stream);
        }

        for (_idx, mut c) in self.combinators {
            stream.append(&mut c.substream);
        }

        stream
    }

    fn merge(&mut self, mut other: Self) {
        self.bound = self.bound.union(other.bound);
        self.combinators.append(&mut other.combinators);
    }

    fn from_stream(combi: Vec<SecondaryStream>) -> Vec<Self> {
        let mut clusters: Vec<Self> = Vec::new();

        for (idx, c) in combi.into_iter().enumerate() {
            let mut candidate_clusters = vec![];

            //Check in which clusters we would fit
            for (cidx, cluster) in clusters.iter_mut().enumerate() {
                if c.tip_info.bbox.is_intersecting(&cluster.bound) {
                    candidate_clusters.push(cidx);
                }
            }

            match candidate_clusters.len() {
                0 => clusters.push(Cluster {
                    bound: c.tip_info.bbox,
                    combinators: vec![(idx, c)],
                    skip_operator: None,
                }), // create new cluster since we are not intersecting at all
                1 => {
                    //Push into existing cluster
                    clusters[candidate_clusters[0]].bound =
                        clusters[candidate_clusters[0]].bound.union(c.tip_info.bbox);
                    clusters[candidate_clusters[0]].combinators.push((idx, c));
                }
                _ => {
                    //Combine clusters that are intersecting into one
                    let mut new_cluster = Cluster {
                        bound: c.tip_info.bbox,
                        combinators: vec![(idx, c)],
                        skip_operator: None,
                    };

                    for candidate in candidate_clusters.into_iter().rev() {
                        let oldc = clusters.remove(candidate);
                        new_cluster.merge(oldc);
                    }

                    clusters.push(new_cluster);
                }
            }
        }

        //TODO Maybe split big clusters on the biggest ops or something?

        //Assure that operation order is still correct within the same cluster
        for c in &mut clusters {
            c.combinators.sort_by_key(|(k, _c)| *k);
        }

        clusters
    }
}

///Inserts skip ops on the start of each cluster, if the clusters complexity is bigger then a sphere|box itersection test.
///In that case the skip test might accelerate evaluation in the interpreter.
fn insert_skip_ops(clusters: &mut Vec<Cluster>) {
    for c in clusters {
        //Currently inserting skip ops when ever we have more then one operation.
        //TODO Insert "per combinator" heuristic, which is compared to the sphere|box intersection test.
        //if c.combinators.len() > 1{
        c.insert_skip();
        //}
    }
}
