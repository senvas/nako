/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
//! TODO: Explain why we are not using serde. Mostly because at runtime we don't know what to expect, so the serialized data has to know which types to build.
//!

use crate::{sdf::BBox, stream::TipInfo};
use glam::{Mat4, Vec3};

pub type Word = u32;

#[derive(Debug, Clone, Copy)]
pub enum OpTy {
    Skip = 0,

    Union = 1,
    SmoothUnion = 2,

    Intersection = 3,
    SmoothIntersection = 4,

    Subtraction = 5,
    SmoothSubtraction = 6,

    Round = 7,
    Onion = 8,

    ///Projections from 2d into 3d.
    ///
    ///This projects a plante into space onto which the 2d
    ///forms are presented
    PlanarProjection = 9,
    ///Uses the 2d sdf as basis to revolute it around the coordiante systems origin
    Revolution = 10,
    ///Extrudes the given 2d sdf, extrudes it down the z axis.
    Extrude = 11,

    ///Adds the following vec3 to the evaluation coordinate used
    CoordAdd = 12,
    ///Multiplies the given coordiante to the evaluation coordinate
    CoordMul = 13,
    ///Multiplies the given matrix (Mat3) to the coordinate.
    CoordMatMul = 14,

    //Result manipulation
    Color = OpTy::CoordMatMul as isize + 1,

    //3d primitives
    Sphere = OpTy::CoordMatMul as isize + 2,
    Plane = OpTy::CoordMatMul as isize + 3,
    Box = OpTy::CoordMatMul as isize + 4,
    Line = OpTy::CoordMatMul as isize + 5,
    Triangle = OpTy::CoordMatMul as isize + 6,

    //2d primitives
    Circle = OpTy::Triangle as isize + 1,
    Box2d = OpTy::Triangle as isize + 2,
    Line2d = OpTy::Triangle as isize + 3,
    Polygon2d = OpTy::Triangle as isize + 4,
    Bezier2d = OpTy::Triangle as isize + 5,

    Invalid = u32::MAX as isize,
}

impl From<u32> for OpTy {
    fn from(t: u32) -> Self {
        match t {
            const { OpTy::Skip as u32 } => OpTy::Skip,

            const { OpTy::Union as u32 } => OpTy::Union,
            const { OpTy::SmoothUnion as u32 } => OpTy::SmoothUnion,

            const { OpTy::Intersection as u32 } => OpTy::Intersection,
            const { OpTy::SmoothIntersection as u32 } => OpTy::SmoothIntersection,

            const { OpTy::Subtraction as u32 } => OpTy::Subtraction,
            const { OpTy::SmoothSubtraction as u32 } => OpTy::SmoothSubtraction,

            const { OpTy::Round as u32 } => OpTy::Round,
            const { OpTy::Onion as u32 } => OpTy::Onion,

            const { OpTy::PlanarProjection as u32 } => OpTy::PlanarProjection,
            const { OpTy::Revolution as u32 } => OpTy::Revolution,
            const { OpTy::Extrude as u32 } => OpTy::Extrude,

            const { OpTy::CoordAdd as u32 } => OpTy::CoordAdd,
            const { OpTy::CoordMul as u32 } => OpTy::CoordMul,
            const { OpTy::CoordMatMul as u32 } => OpTy::CoordMatMul,

            const { OpTy::Color as u32 } => OpTy::Color,

            const { OpTy::Sphere as u32 } => OpTy::Sphere,
            const { OpTy::Plane as u32 } => OpTy::Plane,
            const { OpTy::Box as u32 } => OpTy::Box,
            const { OpTy::Line as u32 } => OpTy::Line,
            const { OpTy::Triangle as u32 } => OpTy::Triangle,

            const { OpTy::Circle as u32 } => OpTy::Circle,
            const { OpTy::Box2d as u32 } => OpTy::Box2d,
            const { OpTy::Line2d as u32 } => OpTy::Line2d,
            const { OpTy::Polygon2d as u32 } => OpTy::Polygon2d,
            const { OpTy::Bezier2d as u32 } => OpTy::Bezier2d,

            _ => OpTy::Invalid,
        }
    }
}

pub struct StreamSerializer {
    pub bbox: BBox,
    pub buffer: Vec<Word>,
}

impl StreamSerializer {
    pub fn new() -> Self {
        StreamSerializer {
            bbox: BBox {
                min: Vec3::ZERO,
                max: Vec3::ZERO,
            },
            buffer: Vec::new(),
        }
    }

    pub fn push_word(&mut self, word: Word) {
        self.buffer.push(word);
    }

    pub fn push_f32(&mut self, f: f32) {
        self.push_word(u32::from_be_bytes(f.to_be_bytes()));
    }

    pub fn push_vec3(&mut self, v: Vec3) {
        self.push_f32(v.x);
        self.push_f32(v.y);
        self.push_f32(v.z);
    }

    ///Pushes a 4x4 matrix in col order
    pub fn push_matrix(&mut self, m: Mat4) {
        let ordered = m.to_cols_array();
        for v in &ordered {
            self.push_f32(*v);
        }
    }

    pub fn append_stream(&mut self, other: &Vec<Word>) {
        self.buffer.append(&mut other.clone())
    }

    pub fn consume(self) -> (TipInfo, Vec<Word>) {
        (TipInfo { bbox: self.bbox }, self.buffer)
    }

    pub fn on_bbox<F>(&mut self, f: F)
    where
        F: FnOnce(&mut BBox),
    {
        f(&mut self.bbox);
    }
}
