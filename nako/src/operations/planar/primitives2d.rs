/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use glam::{Vec2, Vec3};

use crate::{
    sdf::BBox,
    serialize::{OpTy, StreamSerializer, Word},
    stream::Primitive2d,
};

#[derive(Clone, Copy)]
pub struct Circle {
    pub radius: f32,
}

impl Primitive2d for Circle {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Circle as Word);
        ser.push_f32(self.radius);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(
            Vec2::new(-self.radius, -self.radius),
            Vec2::new(self.radius, self.radius),
        )
    }
}

#[derive(Clone, Copy)]
pub struct Box2d {
    pub extent: Vec2,
}

impl Primitive2d for Box2d {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Box2d as Word);
        ser.push_f32(self.extent.x);
        ser.push_f32(self.extent.y);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(self.extent.abs() * -1.0, self.extent.abs())
    }
}

///A 2d line from point a, to b, with a width.
#[derive(Clone, Copy)]
pub struct Line2d {
    pub start: Vec2,
    pub end: Vec2,
    pub width: f32,
}

impl Primitive2d for Line2d {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Line2d as Word);
        ser.push_f32(self.start.x);
        ser.push_f32(self.start.y);
        ser.push_f32(self.end.x);
        ser.push_f32(self.end.y);
        ser.push_f32(self.width);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(
            Vec2::new(self.start.x.min(self.end.x), self.start.y.min(self.end.y)),
            Vec2::new(self.start.x.max(self.end.x), self.start.y.max(self.end.y)),
        )
    }
}

///Polygone made from vertices. Is always closed.
#[derive(Clone)]
pub struct Polygone2d {
    pub vertices: Vec<Vec2>,
}

impl Primitive2d for Polygone2d {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Polygon2d as Word);
        ser.push_word(self.vertices.len() as Word);
        for v in &self.vertices {
            ser.push_f32(v.x);
            ser.push_f32(v.y);
        }
    }

    fn bound(&self) -> BBox {
        let mut min = Vec2::new(f32::INFINITY, f32::INFINITY);
        let mut max = Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY);

        for vert in &self.vertices {
            min = min.min(*vert);
            max = max.max(*vert);
        }

        BBox::new_2d(min, max)
    }
}

#[derive(Clone, Copy)]
pub struct Bezier2d {
    pub start: Vec2,
    ///Controll point through which the line travels from start to end
    pub controll: Vec2,
    pub end: Vec2,
    pub width: f32,
}

impl Primitive2d for Bezier2d {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Bezier2d as Word);
        ser.push_f32(self.start.x);
        ser.push_f32(self.start.y);

        ser.push_f32(self.controll.x);
        ser.push_f32(self.controll.y);

        ser.push_f32(self.end.x);
        ser.push_f32(self.end.y);

        ser.push_f32(self.width);
    }

    //TODO lazy, therfore too big.
    fn bound(&self) -> BBox {
        let min = Vec2::new(
            self.start.x.min(self.controll.x).min(self.end.x) - self.width,
            self.start.y.min(self.controll.y).min(self.end.y) - self.width,
        );

        let max = Vec2::new(
            self.start.x.max(self.controll.x).max(self.end.x) + self.width,
            self.start.y.max(self.controll.y).max(self.end.y) + self.width,
        );

        BBox::new_2d(min, max)
    }
}

///Glsl like sign function. Returns -1 if negative, 0 when 0 and 1 if positive.
#[inline]
pub fn sign(i: f32) -> f32 {
    if i == 0.0 {
        0.0
    } else if i > 0.0 {
        1.0
    } else {
        -1.0
    }
}

pub fn dot2_vec3(i: Vec3) -> f32 {
    i.dot(i)
}

///Creates dot product with self (dot2(a) == a.dot(a))
#[inline]
pub fn dot2(i: Vec2) -> f32 {
    i.dot(i)
}

///Calculates berzier distance from parameters.
#[inline]
pub fn bezier2d(p: Vec2, start: Vec2, ctrl: Vec2, end: Vec2, width: f32) -> f32 {
    let a = ctrl - start;
    let b = start - 2.0 * ctrl + end;
    let c = a * 2.0;
    let d = start - p;

    let kk = 1.0 / b.dot(b);
    let kx = kk * a.dot(b);
    let ky = kk * (2.0 * a.dot(a) + d.dot(b)) / 3.0;
    let kz = kk * d.dot(a);

    let p = ky - kx * kx;
    let p3 = p * p * p;
    let q = kx * (2.0 * kx * kx - 3.0 * ky) + kz;
    let h = q * q + 4.0 * p3;

    let res = if h >= 0.0 {
        //first root
        let h = h.sqrt();
        let x = (Vec2::new(h, -h) - Vec2::new(q, q)) / 2.0;
        let uv = Vec2::new(sign(x.x), sign(x.y))
            * Vec2::new(x.x.abs().powf(1.0 / 3.0), x.y.abs().powf(1.0 / 3.0));
        let t = (uv.x + uv.y - kx).clamp(0.0, 1.0);
        dot2(d + (c + b * t) * t)
    } else {
        //3rd root
        let z = (-p).sqrt();
        let v = (q / (p * z * 2.0)).acos() / 3.0;
        let m = v.cos();
        let n = v.sin() * 1.732050808;
        let t = (Vec3::new(m + m, -n - m, n - m) * z - Vec3::new(kx, kx, kx))
            .clamp(Vec3::ZERO, Vec3::ONE);
        dot2(d + (c + b * t.x) * t.x).min(dot2(d + (c + b * t.y) * t.y))
    };

    res.sqrt() - width
}
