/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
//! Contains all planar (2d) sdf primitives, modifiers and combinators.

pub mod combinators2d;
pub mod modifiers2d;
pub mod primitives2d;
