/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use crate::{
    operations::volumetric::{
        Intersect, SmoothIntersect, SmoothSubtraction, SmoothUnion, Subtraction, Union,
    },
    serialize::{OpTy, StreamSerializer, Word},
    stream::Combinator2d,
};

impl Combinator2d for Union {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Union as Word);
    }
}

impl Combinator2d for SmoothUnion {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothUnion as Word);
        ser.push_f32(self.0);
    }
}

impl Combinator2d for Intersect {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Intersection as Word);
    }
}

impl Combinator2d for SmoothIntersect {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothIntersection as Word);
        ser.push_f32(self.0);
    }
}

impl Combinator2d for Subtraction {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Subtraction as Word);
    }
}

impl Combinator2d for SmoothSubtraction {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothSubtraction as Word);
        ser.push_f32(self.0);
    }
}
