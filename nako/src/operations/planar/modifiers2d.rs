/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use glam::{Mat3, Quat, Vec2};

use crate::{
    operations::volumetric::{modifier::Onion, Color, Round},
    serialize::{OpTy, StreamSerializer, Word},
    stream::Modifier2d,
};

///2d implementation of color
impl Modifier2d for Color {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Color as Word);
        ser.push_vec3(self.0);
    }
}

#[derive(Clone, Copy)]
pub struct Translate2d(pub Vec2);
impl Modifier2d for Translate2d {
    fn pre(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as Word);
        let inv = -self.0;
        ser.push_f32(inv.x);
        ser.push_f32(inv.y);
    }
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as Word);
        ser.push_f32(self.0.x);
        ser.push_f32(self.0.y);
        ser.on_bbox(|bbox| {
            bbox.min += self.0.extend(0.0);
            bbox.max += self.0.extend(0.0);
        })
    }
}

#[derive(Clone, Copy)]
pub struct Transform2d {
    pub translation: Vec2,
    ///Rotation around the current primitives origin in radians (use f32's "to_radians" function to get there from degrees).
    pub rotation: f32,
}

impl Transform2d {
    pub fn to_matrix(&self) -> Mat3 {
        Mat3::from_quat(Quat::from_rotation_z(self.rotation))
            * Mat3::from_translation(self.translation)
    }
}

impl Modifier2d for Transform2d {
    fn pre(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        let mat_inv = mat.inverse();
        ser.push_word(OpTy::CoordMatMul as Word);
        let colmat = mat_inv.to_cols_array_2d();
        ser.push_vec3(colmat[0].into());
        ser.push_vec3(colmat[1].into());
        ser.push_vec3(colmat[2].into());
    }

    fn post(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        ser.push_word(OpTy::CoordMatMul as Word);
        let colmat = mat.to_cols_array_2d();
        ser.push_vec3(colmat[0].into());
        ser.push_vec3(colmat[1].into());
        ser.push_vec3(colmat[2].into());
    }
}

impl Modifier2d for Round {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Round as Word);
        ser.push_f32(self.radius);
    }
}

impl Modifier2d for Onion {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Onion as Word);
        ser.push_f32(self.thickness);
    }
}
