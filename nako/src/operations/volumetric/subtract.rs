/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use crate::{
    operations::mix,
    sdf::{BBox, NakoResult},
    serialize::{OpTy, StreamSerializer, Word},
    stream::Combinator,
};
use glam::Vec3;

///Composes two expressions by taking the minimum
#[derive(Clone, Copy)]
pub struct Subtraction;
impl Combinator for Subtraction {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Subtraction as Word);
    }
    fn bound_modifier(&self, _secondary_bound: &mut BBox) {
        //TODO implement
    }
}
impl Subtraction {
    pub fn evaluate(&self, r1: NakoResult, mut r2: NakoResult) -> NakoResult {
        r2.result *= -1.0;

        if r1.result > r2.result {
            r1
        } else {
            r2
        }
    }
}

///Composes two expressions by taking the minimum smoothing fields within the given radius.
#[derive(Clone, Copy)]
pub struct SmoothSubtraction(pub f32);

impl SmoothSubtraction {
    pub fn evaluate(&self, r1: NakoResult, mut r2: NakoResult) -> NakoResult {
        let h = (0.5 - 0.5 * (r1.result + r2.result) / self.0).clamp(0.0, 1.0);

        r2.result = mix(-r2.result, r1.result, h) + (self.0 * h * (1.0 - h));
        //TODO fix color blending, is currently a little off since its not using the correction the result uses
        //Also currently using subtracting colors, maybe not wanted and uses additive colors as well?
        r2.color = Vec3::new(
            mix(r2.color.x, r1.color.x, h),
            mix(r2.color.y, r1.color.y, h),
            mix(r2.color.z, r1.color.z, h),
        );

        r2
    }
}

impl Combinator for SmoothSubtraction {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothSubtraction as Word);
        ser.push_f32(self.0);
    }

    fn bound_modifier(&self, _secondary_bound: &mut BBox) {
        //TODO Implement modifier, however all we know at this point is, that the bound isnt getting bigger...
    }
}
