/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use glam::Vec3;

use crate::{
    sdf::BBox,
    serialize::{OpTy, StreamSerializer},
    stream::{Combinator, Modifier},
};

///Attaches a color to some SDF. Shows how to implement result depending results.
#[derive(Clone, Copy)]
pub struct Color(pub Vec3);

impl Modifier for Color {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Color as u32);
        ser.push_vec3(self.0);
    }
}

#[derive(Clone, Copy)]
pub struct SkipOperator {
    pub bound: BBox,
    pub skip_to: u32,
}

impl Combinator for SkipOperator {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Skip as u32);
        ser.push_vec3(self.bound.min);
        ser.push_vec3(self.bound.max);
        ser.push_word(self.skip_to);
    }
}
