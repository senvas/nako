/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use crate::{
    operations::mix,
    sdf::{BBox, NakoResult},
    serialize::{OpTy, StreamSerializer, Word},
    stream::Combinator,
};
use glam::Vec3;

///Composes two expressions by taking the minimum
#[derive(Clone, Copy)]
pub struct Intersect;
impl Combinator for Intersect {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Intersection as Word);
    }

    fn bound_modifier(&self, _secondary_bound: &mut BBox) {
        //TODO implement anything?
    }
}
impl Intersect {
    pub fn evaluate(&self, r1: NakoResult, r2: NakoResult) -> NakoResult {
        if r1.result > r2.result {
            r1
        } else {
            r2
        }
    }
}

///Composes two expressions by taking the minimum, smoothes fields in the given radius.
#[derive(Clone, Copy)]
pub struct SmoothIntersect(pub f32);
impl Combinator for SmoothIntersect {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothIntersection as Word);
        ser.push_f32(self.0);
    }

    fn bound_modifier(&self, secondary_bound: &mut BBox) {
        secondary_bound.grow_even(Vec3::new(self.0, self.0, self.0));
    }
}
impl SmoothIntersect {
    pub fn evaluate(&self, mut r1: NakoResult, r2: NakoResult) -> NakoResult {
        let h = (0.5 - 0.5 * (r2.result - r1.result) / self.0).clamp(0.0, 1.0);

        r1.result = mix(r1.result, r2.result, h) + (self.0 * h * (1.0 - h));
        //TODO fix color blending, is currently a little off since its not using the correction the result uses
        //Also currently using additive colors. is wanted?
        r1.color = Vec3::new(
            mix(r1.color.x, r2.color.x, h),
            mix(r1.color.y, r2.color.y, h),
            mix(r1.color.z, r2.color.z, h),
        );

        r1
    }
}
