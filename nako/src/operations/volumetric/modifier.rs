/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use crate::{
    serialize::{OpTy, StreamSerializer, Word},
    stream::Modifier,
};
use glam::{Mat4, Quat, Vec3};
///Translates the inner sdf (by translating the queried point by the inverse)
#[derive(Clone, Copy)]
pub struct Translate(pub Vec3);

impl Modifier for Translate {
    fn pre(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as u32);
        ser.push_vec3(-self.0);
    }

    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as u32);
        ser.push_vec3(self.0);
        ser.on_bbox(|bbox| {
            bbox.min += self.0;
            bbox.max += self.0;
        });
    }
}

///Transformation of in space for some primitive.
#[derive(Clone, Copy)]
pub struct Transform {
    pub translation: Vec3,
    pub rotation: Quat,
}

impl Transform {
    pub fn to_matrix(&self) -> Mat4 {
        Mat4::from_rotation_translation(self.rotation, self.translation)
    }
}

impl Modifier for Transform {
    fn pre(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        let mat_inv = mat.inverse();
        ser.push_word(OpTy::CoordMatMul as Word);
        ser.push_matrix(mat_inv);
    }

    fn post(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        ser.push_word(OpTy::CoordMatMul as Word);
        ser.push_matrix(mat);
        ser.on_bbox(|bbox| {
            bbox.transform(mat);
        });
    }
}

///Rounds the expression with given radius
#[derive(Clone, Copy)]
pub struct Round {
    pub radius: f32,
}
impl Modifier for Round {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Round as Word);
        ser.push_f32(self.radius);
    }
}

///Replaces a volume with a volume that represents the former sdfs walls (f(x) = 0) with a
/// `thickness` thick hull.
#[derive(Clone, Copy)]
pub struct Onion {
    pub thickness: f32,
}
impl Modifier for Onion {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Onion as Word);
        ser.push_f32(self.thickness);
    }
}
