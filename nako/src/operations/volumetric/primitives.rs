/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
//!Contains basic primitives that define, generic over any return type, how an surface is found.
use crate::{
    sdf::BBox,
    serialize::{OpTy, StreamSerializer, Word},
    stream::{PrimaryStream2d, Primitive},
};
use glam::Vec3;

///Sphere with its center at (0,0,0) and the given radius.
#[derive(Clone, Copy, Debug)]
pub struct Sphere {
    pub radius: f32,
}

impl Sphere {
    pub fn evaluate(&self, coord: Vec3) -> f32 {
        coord.length() - self.radius
    }

    fn bounding_box(&self) -> BBox {
        BBox {
            min: Vec3::new(-self.radius, -self.radius, -self.radius),
            max: Vec3::new(self.radius, self.radius, self.radius),
        }
    }
}

impl Primitive for Sphere {
    fn bound(&self) -> BBox {
        self.bounding_box()
    }

    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Sphere as u32);
        ser.push_f32(self.radius);
    }
}

///Simple box starting at (0,0,0) extending until `extent`.
#[derive(Clone, Copy)]
pub struct BoxExact {
    pub extent: Vec3,
}

impl BoxExact {
    pub fn evaluate(&self, coord: Vec3) -> f32 {
        let q = coord.abs() - self.extent;
        (q.max(Vec3::ZERO)).length() + q.max_element().min(0.0)
        //(coord.abs() - self.extent).max_element()
    }
}

impl Primitive for BoxExact {
    fn bound(&self) -> BBox {
        BBox {
            max: self.extent.abs(),
            min: -1.0 * self.extent.abs(),
        }
    }

    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Box as u32);
        ser.push_vec3(self.extent.abs());
    }
}

///Infinit plane facing `normal`. Placed at `hight` on this normal in space. Has an infinit bounding
///box, execpt if one axis on `normal` is 1 and the rest is 0.
#[derive(Clone, Copy)]
pub struct Plane {
    ///Vec3 denoting normal of this plane
    pub normal: Vec3,
    ///location on the y axis in space
    pub height: f32,
}

impl Plane {
    pub fn evaluate(&self, coord: Vec3) -> f32 {
        coord.dot(self.normal.normalize()) - self.height
    }

    pub fn bounding_box(&self) -> BBox {
        //For a plane there are two cases. Either the plane has an ideal direction into +/- x,y,z
        //Or is somehow oriented. In the former case it has no hight on its normal axis, and infinit width/length
        //on its plane axis.

        //In the second case it is infinit in each direction

        //Note matching on floats, however, we can only have a plane on one of the axis aligned planes
        //if two values are zero. So this shouldnt be a problem in floating point land.
        match (
            self.normal.x == 0.0,
            self.normal.y == 0.0,
            self.normal.z == 0.0,
        ) {
            (false, true, true) => {
                //Plane on yz
                BBox {
                    min: Vec3::new(self.height, f32::NEG_INFINITY, f32::NEG_INFINITY),
                    max: Vec3::new(self.height, f32::INFINITY, f32::INFINITY),
                }
            }
            (true, false, true) => {
                //Plane on xz
                BBox {
                    min: Vec3::new(f32::NEG_INFINITY, self.height, f32::NEG_INFINITY),
                    max: Vec3::new(f32::INFINITY, self.height, f32::INFINITY),
                }
            }
            (true, true, false) => {
                //Plane on xy
                BBox {
                    min: Vec3::new(f32::NEG_INFINITY, f32::NEG_INFINITY, self.height),
                    max: Vec3::new(f32::INFINITY, f32::INFINITY, self.height),
                }
            }
            _ => {
                //Something else
                BBox {
                    min: Vec3::new(f32::NEG_INFINITY, f32::NEG_INFINITY, f32::NEG_INFINITY),
                    max: Vec3::new(f32::INFINITY, f32::INFINITY, f32::INFINITY),
                }
            }
        }
    }
}

impl Primitive for Plane {
    fn bound(&self) -> BBox {
        self.bounding_box()
    }

    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Plane as u32);
        ser.push_vec3(self.normal.normalize());
        ser.push_f32(self.height.abs());
    }
}

///Line from a to b with a start and end point.
#[derive(Clone, Copy)]
pub struct Line {
    pub start: Vec3,
    pub end: Vec3,
    pub width: f32,
}

impl Primitive for Line {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Line as Word);
        ser.push_vec3(self.start);
        ser.push_vec3(self.end);
        ser.push_f32(self.width);
    }

    fn bound(&self) -> BBox {
        let width = Vec3::new(self.width, self.width, self.width);
        BBox {
            min: (self.start - width).min(self.end - width),
            max: (self.start + width).max(self.end + width),
        }
    }
}

///Triangle made from the given vertices.
#[derive(Clone, Copy)]
pub struct Triangle {
    pub vertices: [Vec3; 3],
    pub thickness: f32,
}
impl Primitive for Triangle {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Triangle as Word);
        ser.push_f32(self.thickness);
        ser.push_vec3(self.vertices[0]);
        ser.push_vec3(self.vertices[1]);
        ser.push_vec3(self.vertices[2]);
    }

    fn bound(&self) -> BBox {
        let thickness = Vec3::new(self.thickness, self.thickness, self.thickness);
        BBox {
            min: self.vertices[0].min(self.vertices[1]).min(self.vertices[2]) - thickness,
            max: self.vertices[0].max(self.vertices[1]).max(self.vertices[2]) + thickness,
        }
    }
}

///Projects some 2d planar sdf onto a plane in space.
#[derive(Clone)]
pub struct PlanarProjection {
    pub stream: PrimaryStream2d,
}

impl Primitive for PlanarProjection {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::PlanarProjection as Word);
        ser.push_word(self.stream.stream.len() as Word);
        ser.append_stream(&self.stream.stream);
    }

    fn bound(&self) -> BBox {
        //Note that the projection is on the x/z plane
        BBox {
            min: Vec3::new(self.stream.bound.min.x, 0.0, self.stream.bound.min.y),
            max: Vec3::new(self.stream.bound.max.x, 0.0, self.stream.bound.max.y),
        }
    }
}

#[derive(Clone)]
pub struct Extrusion {
    pub stream: PrimaryStream2d,
    pub length: f32,
}

impl Primitive for Extrusion {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Extrude as Word);
        ser.push_f32(self.length);
        ser.push_word(self.stream.stream.len() as u32);
        ser.append_stream(&self.stream.stream);
    }

    fn bound(&self) -> BBox {
        let mut b = self.stream.bound;
        b.max.z = self.length;
        b.min.z = 0.0; // I guess?
        b
    }
}

#[derive(Clone)]
pub struct Revolution {
    pub stream: PrimaryStream2d,
    ///Offset for the revolutions center. Must be positive
    pub offset: f32,
}

impl Primitive for Revolution {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Revolution as Word);
        ser.push_f32(self.offset);
        ser.push_word(self.stream.stream.len() as Word);
        ser.append_stream(&self.stream.stream);
    }

    fn bound(&self) -> BBox {
        //Half bound @ offset = 0.0, otherwise adding offset
        BBox {
            min: Vec3::new(
                self.stream.bound.min.x - self.offset, //Winding the x min around 0.0
                self.stream.bound.min.y, //The y extend on the xy plane of this object is our hight
                self.stream.bound.min.x - self.offset, //Sames as x since we are winding around 0,0 on the xy plane.
            ),
            max: Vec3::new(
                self.stream.bound.max.x + self.offset, //Winding the x min around 0.0
                self.stream.bound.max.y, //The y extend on the xy plane of this object is our hight
                self.stream.bound.max.x + self.offset, //Sames as x since we are winding around 0,0 on the xy plane.
            ),
        }
    }
}
