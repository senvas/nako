/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
use glam::Vec3;

use crate::{
    operations::mix,
    sdf::{BBox, NakoResult},
    serialize::{OpTy, StreamSerializer, Word},
    stream::Combinator,
};

///Composes two expressions by taking the minimum
#[derive(Clone, Copy)]
pub struct Union;
impl Combinator for Union {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Union as Word);
    }
}

///Composes two expressions by taking the minimum, smoothes fields in the given radius.
#[derive(Clone, Copy)]
pub struct SmoothUnion(pub f32);

impl SmoothUnion {
    pub fn evaluate(&self, mut r1: NakoResult, r2: NakoResult) -> NakoResult {
        let h = (0.5 + 0.5 * (r2.result - r1.result) / self.0).clamp(0.0, 1.0);

        r1 = NakoResult {
            result: mix(r1.result, r2.result, h) - (self.0 * h * (1.0 - h)),
            color: Vec3::new(
                mix(r1.color.x, r2.color.x, h),
                mix(r1.color.y, r2.color.y, h),
                mix(r1.color.z, r2.color.z, h),
            ),
        };

        r1
    }
}

impl Combinator for SmoothUnion {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothUnion as Word);
        ser.push_f32(self.0);
    }

    fn bound_modifier(&self, secondary_bound: &mut BBox) {
        secondary_bound.grow_even(Vec3::new(self.0, self.0, self.0)); //Need to grow the bound since the radius in which we are changed grows as well
    }
}
