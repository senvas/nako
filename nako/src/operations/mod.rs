pub mod planar;
pub mod volumetric;

#[inline]
pub fn mix(a: f32, b: f32, alpha: f32) -> f32 {
    let alpha = alpha.clamp(0.0, 1.0);
    (a * alpha) + (b * (1.0 - alpha))
}
