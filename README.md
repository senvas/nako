# Nako
Rust crates for efficient [SDF](https://en.wikipedia.org/wiki/Signed_distance_function) rendering on CPU and GPU with runtime defined function.

## Description

The project is build upon the `nako` main crate, that lets you define signed distance functions. It also contains several already defined standard primitives 
as well as transformations, that should get you started. 

`nako` iteself makes not assumption over the context the sdf is used in. It merly provides you with a standard way of converting those
functions into a defined bytecode. For rendering have a look at `nakorender` which provides two standard renderers.

1. Software rendering on the CPU into an image via the `CpuBackend`
2. Gpu based rendering via the `MarpBackend` using vulkan.


Defining an SDF is done through an operator stream that can be optimized by nako for execution on the cpu or gpu. If you want to implement your own renderer, have a look at the gpu shaders to get an idea.

## Overview
The main idea of nako is to give a user the ability to easyly draw any shape to the screen. The idea is not new and already covered by libraries like SDL.
However, drawing complex shapes usually becomes a nighmare to manage for the user. Usualy because the shapes are based on vertices. Nako instead is build on the concept of Signed-Distance-Fields. Those can be combined in creative ways to draw comlex shapes.

Nako itself only defines a way to create SDF functions which are defined by a custom instruction set. Any interpreter on that instruction set in turn can display (or just calculate) the generated SDFs. Therefore it is easy to create a GPU executor, or integrate the system into a already programmed engine.

To understand the concept a little bit better, have a look at `nakorender/src/cpu` as well as the `serialize` functions of the operations within `nako/src/operations` directory.

## Nako Std
The `nako_std` crate defines some usuful additional data structures on top of nako. For instance button and other interface elements as well as structures to generate character sdfs from fonts.

It however does not make an assumption over the used renderer as well.

## Building and running
### Prerequisite
When using the gpu renderer, make sure that you have a vulkan capable graphics card (anything after ~2015 should be fine).

### Building
Building and using the crates is the usual
```shell
cargo build --release
```

### Running the demo
Running the `layers` crate (which contains a small demo) is done through
```shell
cargo run --release
```

## Documentation
The crates are mostly documented, however since they are not hosted on crates.io yet, you'll have to build the docs yourself via:
```
cargo doc --open
```

## License
The whole project is licensed under MPL v2.0, all contributions will be licensed the same.

