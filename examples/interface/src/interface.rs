use nako::{
    glam::{Vec2, Vec3},
    operations::{
        planar::primitives2d::Line2d,
        volumetric::{Color, Union},
    },
    stream::{PrimaryStream2d, SecondaryStream2d},
};
use nako_std::{
    interface::{
        buttons::{LabeledButton, PushButton},
        events::MetaEvent,
        EventConsumer,
    },
    text::{Character, TextLine},
    Area,
};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo},
    camera::Camera2d,
    marp::MarpBackend,
    winit::event::{ElementState, Event, MouseButton, WindowEvent},
};

const FRAME_WIDTH: f32 = 10.0;

struct Background {
    layer: LayerId2d,
}

fn bg_sdf_for_area(area: Area) -> PrimaryStream2d {
    let mut stream = PrimaryStream2d::new();

    let height = Vec2::new(0.0, area.extent().y / 2.0);
    let top_left = area.from + Vec2::new(FRAME_WIDTH, FRAME_WIDTH);
    let top_right = top_left + Vec2::new(area.extent().x / 2.0, 0.0);

    let bottom_left = top_left + height;
    let bottom_right = top_right + height;

    let width = 2.0;
    let color = Color(Vec3::new(0.7, 0.7, 0.7));
    stream = stream
        .push(
            SecondaryStream2d::new(
                Union,
                Line2d {
                    start: top_left,
                    end: top_right,
                    width,
                },
            )
            .push_mod(color)
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Line2d {
                    start: top_right,
                    end: bottom_right,
                    width,
                },
            )
            .push_mod(color)
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Line2d {
                    start: bottom_right,
                    end: bottom_left,
                    width,
                },
            )
            .push_mod(color)
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Line2d {
                    start: bottom_left,
                    end: top_left,
                    width,
                },
            )
            .push_mod(color)
            .build(),
        );

    stream.build()
}

fn update_bg(id: LayerId2d, area: Area, renderer: &mut MarpBackend) {
    renderer.set_layer_info(
        id.into(),
        LayerInfo {
            extent: (area.extent().x as usize, area.extent().y as usize),
            location: (0, 0),
        },
    );
    renderer.update_camera_2d(
        id,
        Camera2d {
            extent: area.extent(),
            location: Vec2::ZERO,
            rotation: 0.0,
        },
    );
    renderer.update_sdf_2d(id, bg_sdf_for_area(area));
}

impl Background {
    fn new(renderer: &mut MarpBackend) -> Self {
        let start_area = Area::ONE;
        let id = renderer.new_layer_2d();
        update_bg(id, start_area, renderer);
        Background { layer: id }
    }

    fn on_event(&mut self, event: MetaEvent, renderer: &mut MarpBackend) {
        match event {
            MetaEvent::Resize(new_area) => {
                //On resize, rebuild inner layout
                update_bg(self.layer, new_area, renderer);
            }
            _ => {}
        }
    }
}

struct Buttons {
    button_layer: LayerId2d,
    text_layer: LayerId2d,

    button: PushButton<()>,
    text_button: LabeledButton<PushButton<()>>,
}

impl Buttons {
    pub fn new(renderer: &mut MarpBackend) -> Self {
        let mut button =
            PushButton::default().with_ctx(|_, _| println!("Pressed unlabeled button"), ());
        button.on_hover_color = Some(Color(Vec3::new(1.0, 0.5, 0.25)));
        button.color = Color(Vec3::new(0.2, 0.2, 0.2));

        let mut text_sub_button =
            PushButton::default().with_ctx(|_, _| println!("Pressed labeled button"), ());
        text_sub_button.on_hover_color = Some(Color(Vec3::new(1.0, 0.5, 0.25)));
        text_sub_button.color = Color(Vec3::new(0.2, 0.2, 0.2));

        let mut text_button = LabeledButton {
            button: text_sub_button,
            label: TextLine::new_from_path("examples/interface/test_font.ttf", "Button Label")
                .unwrap()
                .with_color(Vec3::ONE)
                .with_size(10.0),
            ..Default::default()
        };

        text_button.button.on_hover_color = Some(Color(Vec3::new(1.0, 0.5, 0.25)));

        Buttons {
            button_layer: renderer.new_layer_2d(),
            text_layer: renderer.new_layer_2d(),

            button,
            text_button,
        }
    }

    pub fn on_event(&mut self, event: MetaEvent, renderer: &mut MarpBackend) {
        //Check if it was a resize, in that case pass the resize on
        match event {
            MetaEvent::Resize(new_area) => {
                println!("Resizing to {:?}", new_area);
                let button_size = Vec2::new(100.0, 60.0);
                let button_origin = Vec2::new(FRAME_WIDTH, FRAME_WIDTH) + Vec2::new(10.0, 20.0);
                self.button.set_area(Area {
                    from: new_area.from + button_origin,
                    to: new_area.from + button_origin + button_size,
                });

                let button_origin =
                    button_origin + Vec2::new(button_size.x, 0.0) + Vec2::new(20.0, 0.0);
                self.text_button.set_area(Area {
                    from: new_area.from + button_origin,
                    to: new_area.from + button_origin + button_size,
                });
                let new_layer_info = LayerInfo {
                    extent: (new_area.extent().x as usize, new_area.extent().y as usize),
                    location: (0, 0).into(),
                };
                renderer.set_layer_info(self.button_layer.into(), new_layer_info);
                renderer.set_layer_info(self.text_layer.into(), new_layer_info);
                let cam = Camera2d {
                    extent: new_area.extent(),
                    location: Vec2::ZERO,
                    rotation: 0.0,
                };
                renderer.update_camera_2d(self.button_layer, cam);
                renderer.update_camera_2d(self.text_layer, cam);
            }
            _ => {}
        }

        self.button.update(event);
        self.text_button.button.update(event);
    }

    pub fn update(&mut self, renderer: &mut MarpBackend) {
        if self.button.state_changed() || self.text_button.button.state_changed() {
            println!("Rerender buttons");
            //Got to change the button layer
            let mut button_stream = PrimaryStream2d::new();
            button_stream = self.button.record_button(button_stream);
            button_stream = self.text_button.record_button(button_stream);
            renderer.update_sdf_2d(self.button_layer, button_stream.build());
        }

        if self.text_button.state_changed() {
            println!("Rerender Text");
            let mut text_stream = PrimaryStream2d::new();
            text_stream = self.text_button.record_label(text_stream);
            renderer.update_sdf_2d(self.text_layer, text_stream.build());
        }
    }
}

///The interfaces caches meta information like the cursor position as well as some logical blocks
///For the different structures we show off.
pub struct Interface {
    area: Area,
    bg: Background,
    buttons: Buttons,

    cursor_pos: Vec2,
}

impl Interface {
    pub fn new(renderer: &mut MarpBackend) -> Self {
        Interface {
            area: Area::ZERO,
            bg: Background::new(renderer),
            buttons: Buttons::new(renderer),
            cursor_pos: (0.0, 0.0).into(),
        }
    }

    pub fn on_event(&mut self, event: &Event<()>, renderer: &mut MarpBackend) {
        //Convert event into meta event
        let new_event = match event {
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                self.area = Area {
                    from: Vec2::ZERO,
                    to: (new_size.width as f32, new_size.height as f32).into(),
                };

                MetaEvent::Resize(self.area)
            }
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                let new_loc = Vec2::new(position.x as f32, position.y as f32);
                self.cursor_pos = new_loc;

                MetaEvent::CursorMoved(self.cursor_pos)
            }
            Event::WindowEvent {
                event: WindowEvent::MouseInput { button, state, .. },
                ..
            } => {
                if button == &MouseButton::Left && state == &ElementState::Pressed {
                    MetaEvent::Select(self.cursor_pos)
                } else {
                    MetaEvent::None
                }
            }

            _ => MetaEvent::None,
        };

        if new_event != MetaEvent::None {
            self.buttons.on_event(new_event, renderer);
            self.bg.on_event(new_event, renderer);
        }
    }

    pub fn get_order(&self) -> Vec<LayerId> {
        vec![
            self.bg.layer.into(),
            self.buttons.button_layer.into(),
            self.buttons.text_layer.into(),
        ]
    }

    pub fn update(&mut self, renderer: &mut MarpBackend) {
        self.buttons.update(renderer);
    }
}
