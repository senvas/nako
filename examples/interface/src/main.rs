use nako::{
    glam::{Vec2, Vec3},
    operations::{
        planar::primitives2d::Box2d,
        volumetric::{Color, Union},
    },
    stream::{PrimaryStream2d, SecondaryStream2d},
};
use nakorender::{
    backend::{Backend, LayerInfo},
    camera::Camera2d,
    marp::MarpBackend,
    winit::{
        event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
    },
};

use crate::interface::Interface;

mod interface;

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    let mut renderer = MarpBackend::new(&window, &event_loop);

    let bglayer = renderer.new_layer_2d();
    renderer.update_sdf_2d(
        bglayer,
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(Union, Box2d { extent: Vec2::ONE })
                    .push_mod(Color(Vec3::new(0.1, 0.1, 0.1)))
                    .build(),
            )
            .build(),
    );

    let mut interface = Interface::new(&mut renderer);

    renderer.set_layer_order(&interface.get_order());

    event_loop.run(move |event, _, control_flow| {
        // ControlFlow::Poll continuously runs the event loop, even if the OS hasn't
        // dispatched any events. This is ideal for games and similar applications.
        *control_flow = ControlFlow::Poll;

        interface.on_event(&event, &mut renderer);

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                println!("Resized info to: {:?}", new_size);
                let canvas_as_info = LayerInfo {
                    extent: (new_size.width as usize, new_size.height as usize),
                    location: (0, 0),
                };
                let layer_camera = Camera2d {
                    extent: (2.0, 2.0).into(),
                    location: Vec2::new(-1.0, -1.0),
                    rotation: 0.0,
                };
                renderer.set_layer_info(bglayer.into(), canvas_as_info.clone());
                renderer.update_camera_2d(bglayer, layer_camera.clone());

                renderer.resize(&window);
            }
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Released,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    },
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                interface.update(&mut renderer);
                renderer.render(&window);
            }
            _ => (),
        }
    });
}
