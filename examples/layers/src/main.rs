use std::time::Instant;

use camera_controller::CameraController;
use model_generator::{
    background_grid, combinators2d, jing_jang, random_cloud_in_range, sculpture,
};
//use model_generator::left_operation_patch;
use nako::{
    glam::{Quat, Vec2, Vec3},
    operations::{
        planar::{
            modifiers2d::{Transform2d, Translate2d},
            primitives2d::{Bezier2d, Box2d, Circle, Line2d, Polygone2d},
        },
        volumetric::{
            modifier::{Onion, Transform},
            primitives::{Extrusion, Line, PlanarProjection, Revolution, Triangle},
            BoxExact, Color, Intersect, Plane, Round, SmoothIntersect, SmoothSubtraction,
            SmoothUnion, Sphere, Subtraction, Translate, Union,
        },
    },
    stream::{PrimaryStream, PrimaryStream2d, SecondaryStream, SecondaryStream2d},
};

use nakorender::{
    backend::{Backend, LayerInfo},
    camera::{Camera, Camera2d},
    cpu::CpuBackend,
    marp::MarpBackend,
    winit::{
        event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
    },
};

mod camera_controller;
mod model_generator;

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    let mut renderer = MarpBackend::new(&window, &event_loop);

    let idblobs = renderer.new_layer();
    let sdf = PrimaryStream::new()
        .push(
            SecondaryStream::new(Union, BoxExact { extent: Vec3::ONE })
                .push_mod(Color(Vec3::ONE))
                .build(),
        )
        .build();
    renderer.update_sdf(idblobs, sdf);
    renderer.set_layer_info(
        idblobs.into(),
        LayerInfo {
            extent: (1024, 960),
            location: (100, 100),
        },
    );
    renderer.update_camera(
        idblobs,
        Camera {
            location: Vec3::new(0.0, 0.0, -5.0),
            ..Default::default()
        },
    );

    //background color
    let bg_sdf = PrimaryStream2d::new()
        .push(
            SecondaryStream2d::new(
                Union,
                Box2d {
                    extent: Vec2::new(1.0, 1.0),
                },
            )
            .push_mod(Color(Vec3::new(0.05, 0.045, 0.045)))
            .push_mod(Translate2d(Vec2::new(0.5, 0.5)))
            .build(),
        )
        .build();
    let idbg = renderer.new_layer_2d();
    renderer.update_camera_2d(
        idbg,
        Camera2d {
            extent: Vec2::new(1.0, 1.0),
            location: Vec2::ZERO,
            rotation: 0.0,
        },
    );
    renderer.update_sdf_2d(idbg, bg_sdf);

    //Background grid
    let bg_grid = background_grid(1.0, 1.0);
    let idbg_grid = renderer.new_layer_2d();
    renderer.update_sdf_2d(idbg_grid, bg_grid);
    renderer.update_camera_2d(
        idbg_grid,
        Camera2d {
            extent: Vec2::new(1.0, 1.0),
            location: Vec2::ZERO,
            rotation: 0.0,
        },
    );

    let height = 10.0;
    let thicc = 1.0;
    let width = 8.0;
    //3d cube
    let sdf_3d = PrimaryStream::new()
        .push(
            SecondaryStream::new(
                Union,
                BoxExact {
                    extent: Vec3::new(10.0, 5.0, 3.0),
                },
            )
            .push_mod(Color(Vec3::new(0.8, 0.14, 0.11)))
            .push_mod(Translate(Vec3::new(10.0, 0.0, 25.0)))
            .build(),
        )
        .push(
            SecondaryStream::new(
                SmoothUnion(2.0),
                BoxExact {
                    extent: Vec3::new(5.0, 5.0, 5.0),
                },
            )
            .push_mod(Color(Vec3::new(0.72, 0.74, 0.14)))
            .push_mod(Translate(Vec3::new(0.0, 7.0, 25.0)))
            .push_mod(Round { radius: 1.0 })
            .build(),
        )
        .push(
            SecondaryStream::new(
                SmoothUnion(5.0),
                Extrusion {
                    length: 4.0,
                    stream: PrimaryStream2d::new()
                        .push(
                            SecondaryStream2d::new(
                                Union,
                                Line2d {
                                    start: Vec2::new(-10.0, 0.0),
                                    end: Vec2::new(0.0, 10.0 + thicc),
                                    width: 0.5,
                                },
                            )
                            .push_mod(Color(Vec3::new(0.27, 0.52, 0.53)))
                            .build(),
                        )
                        .push(
                            SecondaryStream2d::new(
                                Union,
                                Polygone2d {
                                    vertices: vec![
                                        Vec2::new(0.0, height + thicc),
                                        Vec2::new(0.0, height),
                                        Vec2::new(width / 2.0 - thicc / 2.0, height),
                                        Vec2::new(width / 2.0 - thicc / 2.0, 0.0),
                                        Vec2::new(width / 2.0 + thicc / 2.0, 0.0),
                                        Vec2::new(width / 2.0 + thicc / 2.0, height),
                                        Vec2::new(width, height),
                                        Vec2::new(width, height + thicc),
                                    ],
                                },
                            )
                            .push_mod(Color(Vec3::new(0.27, 0.52, 0.53)))
                            .build(),
                        )
                        .build(),
                },
            )
            .push_mod(Transform {
                rotation: Quat::from_rotation_x((20.0f32).to_radians()),
                translation: Vec3::new(20.0, -5.0, 20.0),
            })
            .build(),
        )
        .build();

    let id3d = renderer.new_layer();
    renderer.update_sdf(id3d, sdf_3d);

    //Frame
    let sdf_2d = PrimaryStream2d::new()
        .push(
            SecondaryStream2d::new(
                Union,
                Box2d {
                    extent: Vec2::new(0.5, 0.5),
                },
            )
            .push_mod(Color(Vec3::new(0.95, 0.65, 0.05)))
            .push_mod(Translate2d(Vec2::new(0.5, 0.5)))
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Subtraction,
                Box2d {
                    extent: Vec2::new(0.49, 0.49),
                },
            )
            .push_mod(Translate2d(Vec2::new(0.5, 0.5)))
            .build(),
        )
        .build();
    let id2d = renderer.new_layer_2d();
    renderer.update_camera_2d(
        id2d,
        Camera2d {
            extent: Vec2::new(1.0, 1.0),
            location: Vec2::ZERO,
            rotation: 0.0,
        },
    );
    renderer.update_sdf_2d(id2d, sdf_2d);

    //JingJang
    let idjingjang = renderer.new_layer_2d();
    renderer.update_sdf_2d(idjingjang, jing_jang(10.0));

    let idtest = renderer.new_layer_2d();
    renderer.update_sdf_2d(
        idtest,
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(
                    Union,
                    Box2d {
                        extent: Vec2::new(0.3, 0.3),
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .push_mod(Round { radius: 0.2 })
                .push_mod(Translate2d(Vec2::new(0.5, 0.5)))
                .build(),
            )
            .build(),
    );

    //Setup layer drawing order
    renderer.set_layer_order(&[
        idbg.into(),
        idbg_grid.into(),
        idblobs.into(),
        id3d.into(),
        id2d.into(),
        idjingjang.into(),
        idtest.into(),
    ]);

    #[allow(unused_mut)]
    let mut camera = CameraController::default();

    let start = Instant::now();

    event_loop.run(move |event, _, control_flow| {
        // ControlFlow::Poll continuously runs the event loop, even if the OS hasn't
        // dispatched any events. This is ideal for games and similar applications.
        *control_flow = ControlFlow::Poll;

        //Update camera controller
        camera.event(&event);

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                println!("Resized info to: {:?}", new_size);
                let canvas_as_info = LayerInfo {
                    extent: (new_size.width as usize, new_size.height as usize),
                    location: (0, 0),
                };
                renderer.set_layer_info(
                    idblobs.into(),
                    LayerInfo {
                        extent: (new_size.width as usize / 4, new_size.height as usize / 4),
                        location: (new_size.width as usize - new_size.width as usize / 4, 0),
                    },
                );
                //resize background grid
                renderer.update_sdf_2d(
                    idbg_grid,
                    background_grid(new_size.width as f32, new_size.height as f32),
                );
                renderer.update_camera_2d(
                    idbg_grid,
                    Camera2d {
                        extent: Vec2::new(new_size.width as f32, new_size.height as f32),
                        location: Vec2::ZERO,
                        rotation: 0.0,
                    },
                );

                //Resize jing jang
                let jjsize = new_size.width.min(new_size.height) as f32 / 5.0;
                renderer.update_sdf_2d(idjingjang, jing_jang(jjsize));
                renderer.update_camera_2d(
                    idjingjang,
                    Camera2d {
                        extent: Vec2::new(jjsize, jjsize),
                        location: Vec2::ZERO,
                        rotation: 0.0,
                    },
                );

                renderer.set_layer_info(idbg.into(), canvas_as_info);
                renderer.set_layer_info(idbg_grid.into(), canvas_as_info);
                renderer.set_layer_info(id3d.into(), canvas_as_info);
                renderer.set_layer_info(id2d.into(), canvas_as_info);
                renderer.set_layer_info(
                    idjingjang.into(),
                    LayerInfo {
                        extent: (jjsize as usize, jjsize as usize),
                        location: (10, 10),
                    },
                );

                renderer.set_layer_info(
                    idtest.into(),
                    LayerInfo {
                        extent: (50, 50),
                        location: (jjsize as usize + 10, 0),
                    },
                );

                renderer.resize(&window);
            }
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Released,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    },
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                renderer.update_sdf(idblobs, time_based_sdf(start.elapsed().as_secs_f32()));

                camera.update();
                renderer.update_camera(id3d, camera.as_camera());
                renderer.render(&window);
            }
            _ => (),
        }
    });
}

//Frame based sdf
fn time_based_sdf(time: f32) -> PrimaryStream {
    let cube_pos = Vec3::new(time.sin() * 10.0, 0.0, 2.0);
    let cube2_pos = Vec3::new(0.0, 0.0, (time / 2.0).cos() * 4.0);

    PrimaryStream::new()
        .push(
            SecondaryStream::new(
                Union,
                Plane {
                    height: 0.0,
                    normal: Vec3::Y,
                },
            )
            .push_mod(Color(Vec3::ONE))
            .push_mod(Translate(Vec3::new(0.0, -10.0, 0.0)))
            .build(),
        )
        .push(
            SecondaryStream::new(Union, Sphere { radius: 2.0 })
                .push_mod(Color(Vec3::ONE))
                .push_mod(Onion { thickness: 0.25 })
                .build(),
        )
        .push(
            SecondaryStream::new(
                SmoothUnion(5.0),
                BoxExact {
                    extent: Vec3::new(1.0, 2.0, 2.0),
                },
            )
            .push_mod(Color(Vec3::new(0.7, 0.68, 0.2)))
            .push_mod(Translate(cube_pos))
            .push_mod(Round { radius: 1.0 })
            .build(),
        )
        .push(
            SecondaryStream::new(
                SmoothSubtraction(1.0),
                BoxExact {
                    extent: Vec3::new(2.0, 1.0, 2.0),
                },
            )
            .push_mod(Color(Vec3::new(0.5, 1.0, 0.2)))
            .push_mod(Translate(cube2_pos))
            .build(),
        )
        .build()
}
