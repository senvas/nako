use nako::{
    glam::{Vec2, Vec3},
    operations::{
        planar::{
            modifiers2d::Translate2d,
            primitives2d::{Bezier2d, Box2d, Circle, Line2d},
        },
        volumetric::{
            primitives::PlanarProjection, BoxExact, Color, Intersect, Plane, SmoothSubtraction,
            SmoothUnion, Sphere, Subtraction, Translate, Union,
        },
    },
    stream::{
        PrimaryStream, PrimaryStream2d, PrimaryStreamBuilder, SecondaryStream, SecondaryStream2d,
    },
};

use dicetest::{Prng, Seed};

//Random translation in range 0..1
fn random_translation(prng: &mut Prng) -> Vec3 {
    Vec3::new(
        prng.next_number() as f32 / u64::MAX as f32,
        prng.next_number() as f32 / u64::MAX as f32,
        prng.next_number() as f32 / u64::MAX as f32,
    )
    .abs()
}

fn rekursive(rng: &mut Prng, count: usize, min: Vec3, max: Vec3) -> PrimaryStreamBuilder {
    let location = min + random_translation(rng) * (max - min);

    if count == 0 {
        let mut builder = PrimaryStream::new();

        builder = builder.push(
            SecondaryStream::new(Union, Sphere { radius: 10.0 })
                .push_mod(Translate(location))
                .push_mod(Color(Vec3::new(0.8, 0.8, 0.8)))
                .build(),
        );

        builder
    } else {
        let mut builder = rekursive(rng, count - 1, min, max);
        builder = builder.push(
            SecondaryStream::new(
                SmoothUnion(10.0),
                BoxExact {
                    extent: Vec3::new(10.0, 5.0, 7.0),
                },
            )
            .push_mod(Translate(location))
            .push_mod(Color(Vec3::new(0.8, 0.8, 0.8)))
            .build(),
        );

        builder
    }
}

pub fn random_cloud_in_range(count: usize, min: Vec3, max: Vec3) -> PrimaryStream {
    //Pseudo random generator
    let mut prng = Prng::from_seed(Seed(42));
    let builder = rekursive(&mut prng, count, min, max);

    builder.build()
}

pub fn sculpture() -> PrimaryStream {
    let mut stream = PrimaryStream::new();

    let walls = Vec3::new(20.0, 2.0, 3.0);
    let p = Vec3::new(-walls.x / 2.0, -100.0, 100.0);
    let color = Vec3::new(0.7, 0.5, 0.1);

    //Lower plane
    stream = stream
        .push(
            SecondaryStream::new(
                Union,
                Plane {
                    height: -100.0,
                    normal: Vec3::Y,
                },
            )
            .push_mod(Color(Vec3::new(0.95, 0.8, 0.8)))
            .build(),
        )
        .push(
            //Frontwall
            SecondaryStream::new(Union, BoxExact { extent: walls })
                .push_mod(Translate(p))
                .push_mod(Color(color))
                .build(),
        )
        .push(
            //Right
            SecondaryStream::new(
                Union,
                BoxExact {
                    extent: Vec3::new(walls.z, walls.y, walls.x),
                },
            )
            .push_mod(Translate(
                p + Vec3::new(walls.x - walls.z, 0.0, walls.x + walls.z),
            ))
            .push_mod(Color(color))
            .build(),
        )
        .push(
            //Back
            SecondaryStream::new(Union, BoxExact { extent: walls })
                .push_mod(Translate(p + Vec3::new(-2.0 * walls.z, 0.0, 2.0 * walls.x)))
                .push_mod(Color(color))
                .build(),
        )
        .push(
            //Left
            SecondaryStream::new(
                Union,
                BoxExact {
                    extent: Vec3::new(walls.z, walls.y, walls.x),
                },
            )
            .push_mod(Translate(
                p + Vec3::new(-walls.x - walls.z, 0.0, walls.x - walls.z),
            ))
            .push_mod(Color(color))
            .build(),
        )
        .push(
            SecondaryStream::new(SmoothUnion(10.0), Sphere { radius: 25.0 })
                .push_mod(Translate(Vec3::new(-250.0, -85.0, 1.0)))
                .push_mod(Color(Vec3::new(0.2, 1.0, 0.2)))
                .build(),
        )
        .push(
            SecondaryStream::new(SmoothUnion(50.0), Sphere { radius: 30.0 })
                .push_mod(Translate(Vec3::new(-250.0, -100.0, 25.0)))
                .push_mod(Color(Vec3::new(0.75, 0.0, 0.2)))
                .build(),
        )
        .push(
            SecondaryStream::new(SmoothUnion(50.0), Sphere { radius: 45.0 })
                .push_mod(Translate(Vec3::new(-250.0, -100.0, 160.0)))
                .push_mod(Color(Vec3::new(0.5, 1.0, 1.0)))
                .build(),
        );

    stream.build()
}

pub fn combinators2d() -> SecondaryStream {
    SecondaryStream::new(
        Union,
        PlanarProjection {
            stream: PrimaryStream2d::new()
                //Normal Combination
                .push(
                    //Intersect
                    SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                        .push_mod(Color(Vec3::ONE))
                        .push_mod(Translate2d(Vec2::new(-10.0, 0.0)))
                        .build(),
                )
                .push(
                    SecondaryStream2d::new(Intersect, Circle { radius: 2.0 })
                        .push_mod(Translate2d(Vec2::new(-8.0, 0.0)))
                        .build(),
                )
                .push(
                    //Union
                    SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                        .push_mod(Color(Vec3::ONE))
                        .push_mod(Translate2d(Vec2::new(10.0, 0.0)))
                        .build(),
                )
                .push(
                    SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                        .push_mod(Translate2d(Vec2::new(8.0, 0.0)))
                        .build(),
                )
                .push(
                    //Subtract
                    SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                        .push_mod(Color(Vec3::ONE))
                        .push_mod(Translate2d(Vec2::new(0.0, 0.0)))
                        .build(),
                )
                .push(
                    SecondaryStream2d::new(Subtraction, Circle { radius: 2.0 })
                        .push_mod(Translate2d(Vec2::new(-2.0, 0.0)))
                        .build(),
                )
                //Smooth Combination
                /*.push(//Intersect
                    SecondaryStream2d::new(Union, Circle{radius: 2.0})
                        .push_mod(Color(Vec3::ONE))
                        .push_mod(Translate2d(Vec2::new(-10.0, 0.0)))
                        .build()
                ).push(
                    SecondaryStream2d::new(Intersect, Circle{radius: 2.0})
                        .push_mod(Translate2d(Vec2::new(-8.0, 0.0)))
                        .build()
                )*/
                .push(
                    //Union
                    SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                        .push_mod(Color(Vec3::ONE))
                        .push_mod(Translate2d(Vec2::new(10.0, 10.0)))
                        .build(),
                )
                .push(
                    SecondaryStream2d::new(SmoothUnion(0.5), Circle { radius: 2.0 })
                        .push_mod(Translate2d(Vec2::new(8.0, 10.0)))
                        .build(),
                )
                .push(
                    //Subtract
                    SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                        .push_mod(Color(Vec3::ONE))
                        .push_mod(Translate2d(Vec2::new(0.0, 10.0)))
                        .build(),
                )
                .push(
                    SecondaryStream2d::new(SmoothSubtraction(0.5), Circle { radius: 2.0 })
                        .push_mod(Translate2d(Vec2::new(-2.0, 10.0)))
                        .build(),
                )
                .build(),
        },
    )
    .push_mod(Color(Vec3::ONE))
    .build()
}

pub fn background_grid(width: f32, height: f32) -> PrimaryStream2d {
    let grid_spacing = 50.0;
    let line_width = 1.0;
    let color = Vec3::new(0.5, 0.45, 0.45);

    let mut builder = PrimaryStream2d::new();

    let mut x = 0.0;
    while x < width {
        //Add vertical line
        builder = builder.push(
            SecondaryStream2d::new(
                Union,
                Line2d {
                    start: Vec2::new(x, 0.0),
                    end: Vec2::new(x, height),
                    width: line_width,
                },
            )
            .push_mod(Color(color))
            .build(),
        );

        x += grid_spacing;
    }

    let mut y = 0.0;
    while y < height {
        //Add vertical line
        builder = builder.push(
            SecondaryStream2d::new(
                Union,
                Line2d {
                    start: Vec2::new(0.0, y),
                    end: Vec2::new(width, y),
                    width: line_width,
                },
            )
            .push_mod(Color(color))
            .build(),
        );

        y += grid_spacing;
    }

    builder.build()
}

///Creates a fake jing jang symbol at offset
pub fn jing_jang(size: f32) -> PrimaryStream2d {
    let color = Vec3::new(0.27, 0.52, 0.53);
    let edge_width = 4.0;
    let roundness = 10.0;
    let circle_ratio = 8.0;

    PrimaryStream2d::new()
        //Circle
        .push(
            SecondaryStream2d::new(Union, Circle { radius: size / 2.0 })
                .push_mod(Translate2d(Vec2::new(size / 2.0, size / 2.0)))
                .push_mod(Color(color))
                .build(),
        )
        .push(
            SecondaryStream2d::new(
                Subtraction,
                Circle {
                    radius: size / 2.0 - edge_width,
                },
            )
            .push_mod(Translate2d(Vec2::new(size / 2.0, size / 2.0)))
            .push_mod(Color(color))
            .build(), //SCurve
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Bezier2d {
                    start: Vec2::new(size / 2.0, 0.0),
                    controll: Vec2::new(size / roundness, size / 4.0),
                    end: Vec2::new(size / 2.0, size / 2.0),
                    width: edge_width / 2.0,
                },
            )
            .push_mod(Color(color))
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Bezier2d {
                    start: Vec2::new(size / 2.0, size / 2.0),
                    controll: Vec2::new(size - (size / roundness), (size / 2.0) + (size / 4.0)),
                    end: Vec2::new(size / 2.0, size),
                    width: edge_width / 2.0,
                },
            )
            .push_mod(Color(color))
            .build(), //Upper right dot
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Circle {
                    radius: size / circle_ratio,
                },
            )
            .push_mod(Color(color))
            .push_mod(Translate2d(Vec2::new(
                size / 2.0 + size / circle_ratio,
                size / 4.0,
            )))
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Subtraction,
                Circle {
                    radius: size / circle_ratio - edge_width,
                },
            )
            .push_mod(Translate2d(Vec2::new(
                size / 2.0 + size / circle_ratio,
                size / 4.0,
            )))
            .build(),
        )
        .push(
            SecondaryStream2d::new(
                Union,
                Circle {
                    radius: size / circle_ratio,
                },
            )
            .push_mod(Color(color))
            .push_mod(Translate2d(Vec2::new(
                size / 2.0 - size / circle_ratio,
                size - size / 4.0,
            )))
            .build(),
        )
        .build()
}
